﻿Imports TESTAMI.SCAPI
Imports System.Text
Imports System.Runtime.InteropServices
Imports Newtonsoft.Json
Imports System.IO

Public Class Frm_AMI
    Dim list_Reader As String
    Dim status As Integer = -99
    Dim returnCode As Integer = 99
    'Dim ReturnValue As Short = 99
    Dim scapi_stt As New SCAPI_STATUS
    Dim ami_stt As New AMI_STATUS
    Dim TextBoxPID As String
    Dim TextBoxCID As String
    'Dim LKOfficeCode As String
    Dim TextBoxOutput As String
    Dim TextBoxKEY As String
    Dim TextBox4 As String
    Dim TextBox5 As String
    Dim TextBoxTKey As String = ""

    Dim namePic As String = ""


    Dim img As Bitmap

    Dim Panel2 As Panel

    Dim LKOfficeCode As String = Configuration.ConfigurationManager.AppSettings.Get("LKOfficeCode").ToString()


    Dim rc9080 As SCAPI.Recive9080
    Dim rc9081 As SCAPI.Recive9081
    Private Sub Frm_AMI_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        checKProcessIM()
        listReader()
        clearData()

    End Sub

    Private Sub getDateActive()
        Dim now As DateTime = DateTime.Now
        Dim d As String = now.ToString("dd")
        Dim m As String = now.ToString("MM")
        Dim y As String = now.ToString("yyyy")
        Dim time As String = now.ToString("HH:mm:ss")
        Dim dateT As String = d & " " & GenMonthThai(m, 2) & " " & (Convert.ToInt32(y) + 543) & " " & time
        dateActive.Text = dateT

    End Sub

    Private Sub listReader()
        list_Reader = ""
        list_Reader = Space(1000) ' จองหน่วยความจำสำหรับเก็บชื่อ Reader ที่ได้
        'LKOfficeCode = "00023"
        returnCode = SCAPI.ListReader(list_Reader, status)
        'returnCode = 0
        If (returnCode = 0) Then
            list_Reader = list_Reader.Trim()
            ' ตัดคำชื่อ Reader เพิ่มเติม หากมีเครื่องอ่านมากกว่า 1 เครื่อง
            While list_Reader.Length > 0
                If (list_Reader <> vbNullChar) Then
                    Dim nn As String = list_Reader.Substring(0, 2)
                    Dim ll As Integer = Integer.Parse(nn)

                    ComboBox1.Items.Add(list_Reader.Substring(2, ll))
                    ComboBox1.SelectedIndex = 0
                    list_Reader = list_Reader.Substring(ll + 2)
                Else
                    list_Reader = ""
                End If

            End While
            Button5.Enabled = True
            Button1.Enabled = True
        Else
            'MessageBox.Show("Return Code = [" & returnCode & " ] " & scapi_stt.GetStatus(returnCode) & Environment.NewLine & "Status Code = [ " & status & " ] " & scapi_stt.GetStatus(status), Text & " ตรวจสอบเครื่องอ่านบัตร")
            MessageBox.Show("ตรวจสอบเครื่องอ่านบัตร")
            ComboBox1.Items.Clear()
            Button5.Enabled = False
            Button1.Enabled = False
            'Close()
        End If
    End Sub

    Public Function GenMonthThai(ByVal intMonth As Integer, ByVal intParameter As Integer)

        Dim strMonth As String = String.Empty

        Select Case intMonth.ToString

            Case "1"
                If intParameter = 1 Then
                    strMonth = "ม.ค"
                ElseIf intParameter = 2 Then
                    strMonth = "มกราคม"
                End If


            Case "2"
                If intParameter = 1 Then
                    strMonth = "ก.พ"
                ElseIf intParameter = 2 Then
                    strMonth = "กุมภาพันธ์"
                End If


            Case "3"
                If intParameter = 1 Then
                    strMonth = "มี.ค"
                ElseIf intParameter = 2 Then
                    strMonth = "มีนาคม"
                End If


            Case "4"
                If intParameter = 1 Then
                    strMonth = "เม.ษ"
                ElseIf intParameter = 2 Then
                    strMonth = "เมษายน"
                End If


            Case "5"
                If intParameter = 1 Then
                    strMonth = "พ.ค"
                ElseIf intParameter = 2 Then
                    strMonth = "พฤษภาคม"
                End If


            Case "6"
                If intParameter = 1 Then
                    strMonth = "มิ.ย"
                ElseIf intParameter = 2 Then
                    strMonth = "มิถุนายน"
                End If


            Case "7"
                If intParameter = 1 Then
                    strMonth = "ก.ค"
                ElseIf intParameter = 2 Then
                    strMonth = "กรกฏาคม"
                End If


            Case "8"
                If intParameter = 1 Then
                    strMonth = "ส.ค"
                ElseIf intParameter = 2 Then
                    strMonth = "สิงหาคม"
                End If


            Case "9"
                If intParameter = 1 Then
                    strMonth = "ก.ย"
                ElseIf intParameter = 2 Then
                    strMonth = "กันยายน"
                End If


            Case "10"
                If intParameter = 1 Then
                    strMonth = "ต.ค"
                ElseIf intParameter = 2 Then
                    strMonth = "ตุลาคม"
                End If


            Case "11"
                If intParameter = 1 Then
                    strMonth = "พ.ย"
                ElseIf intParameter = 2 Then
                    strMonth = "พฤศจิกายน"
                End If


            Case "12"
                If intParameter = 1 Then
                    strMonth = "ธ.ค"
                ElseIf intParameter = 2 Then
                    strMonth = "ธันวาคม"
                End If


        End Select

        Return strMonth
    End Function

    Public Function GenMonthEn(ByVal intMonth As Integer, ByVal intParameter As Integer)

        Dim strMonth As String = String.Empty

        Select Case intMonth.ToString

            Case "1"
                If intParameter = 1 Then
                    strMonth = "Jan"
                ElseIf intParameter = 2 Then
                    strMonth = "January"
                End If


            Case "2"
                If intParameter = 1 Then
                    strMonth = "Feb"
                ElseIf intParameter = 2 Then
                    strMonth = "February"
                End If


            Case "3"
                If intParameter = 1 Then
                    strMonth = "Mar"
                ElseIf intParameter = 2 Then
                    strMonth = "March"
                End If


            Case "4"
                If intParameter = 1 Then
                    strMonth = "Apr"
                ElseIf intParameter = 2 Then
                    strMonth = "April"
                End If


            Case "5"
                If intParameter = 1 Then
                    strMonth = "May"
                ElseIf intParameter = 2 Then
                    strMonth = "May"
                End If


            Case "6"
                If intParameter = 1 Then
                    strMonth = "Jun"
                ElseIf intParameter = 2 Then
                    strMonth = "June"
                End If


            Case "7"
                If intParameter = 1 Then
                    strMonth = "Jul"
                ElseIf intParameter = 2 Then
                    strMonth = "July"
                End If


            Case "8"
                If intParameter = 1 Then
                    strMonth = "Aug"
                ElseIf intParameter = 2 Then
                    strMonth = "August"
                End If


            Case "9"
                If intParameter = 1 Then
                    strMonth = "Oct"
                ElseIf intParameter = 2 Then
                    strMonth = "September"
                End If


            Case "10"
                If intParameter = 1 Then
                    strMonth = "Oct"
                ElseIf intParameter = 2 Then
                    strMonth = "October"
                End If


            Case "11"
                If intParameter = 1 Then
                    strMonth = "Nov"
                ElseIf intParameter = 2 Then
                    strMonth = "November"
                End If


            Case "12"
                If intParameter = 1 Then
                    strMonth = "Dec"
                ElseIf intParameter = 2 Then
                    strMonth = "December"
                End If


        End Select

        Return strMonth
    End Function

    Private Sub showImage(jsonInput As String)
        'Throw New NotImplementedException()
        Dim frm As FormIMG = New FormIMG
        Dim img As LK_00023_01_038 = New LK_00023_01_038
        img = JsonConvert.DeserializeObject(Of LK_00023_01_038)(jsonInput)

        Dim imgg As Byte() = Convert.FromBase64String(img.image)
        Dim mm As New IO.MemoryStream(imgg)
        PictureBox1.Image = Image.FromStream(mm)
    End Sub


    Private Sub send5000()
        'Dim DataRetun = requestCode5000()
        'Return

        Dim send_data As New Get5000
        Dim reply_data As New Reply5000


        Dim ReturnValue As Short
        Dim send_datasize As Integer
        Dim reply_datasize As Integer
        Dim reply_datamax As Integer
        Dim timeOut As Integer
        Dim status As Integer
        Try
            ReturnValue = 99
            status = 0

            reply_datasize = Marshal.SizeOf(reply_data)
            reply_datamax = Marshal.SizeOf(reply_data)

            send_data.Code = Encoding.Default.GetBytes("5000")
            send_data.XKey = Encoding.Default.GetBytes(TextBoxTKey.Trim())

            send_data.OfficeCode = Encoding.Default.GetBytes("00213") ' สน.บท.
            send_data.VersionCode = Encoding.Default.GetBytes("01") ' Version 1
            send_data.ServiceCode = Encoding.Default.GetBytes("038") ' ภาพใบหน้า
            send_data.PID = Encoding.Default.GetBytes(TextBoxPIDex.Text.Trim())

            send_datasize = Marshal.SizeOf(send_data)
            timeOut = 30
            ReturnValue = AMI_REQUEST("", send_data, send_datasize, reply_data, reply_datamax, reply_datasize, timeOut, status)
            Dim stc As String = ""
            stc = scapi_stt.GetStatus(status)
            If stc.Substring(0, "Unknow Status".Length) = "Unknow Status" Then
                stc = ami_stt.GetStatus(status)
            Else
                stc = scapi_stt.GetStatus(status)
            End If

            showImage(Encoding.UTF8.GetString(reply_data.Data).Trim())

        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        RStatus.Text = ""
        Dim typeS As String = ""

        If RadioButton2.Checked Then
            If (TextBoxPIDex.Text <> "" And TextBoxPIDex.Text.Length = 13) Then
                typeS = "0101"
            ElseIf (TextBoxPIDex.Text.Length < 13 And TextBoxPIDex.Text <> "") Then
                MessageBox.Show("กรุณาระบุ เลขประจำตัวประชาชนให้ครบ 13 หลัก")
            Else
                MessageBox.Show("กรุณาระบุ เลขประจำตัวประชาชน")
            End If
        Else
            If (FNAME.Text <> "" And LNAME.Text <> "") Then
                typeS = "0102"
            ElseIf (TextBoxPIDex.Text <> "") Then
                MessageBox.Show("กรุณาระบุ ชื่อ/นามสกุล")
            End If
        End If

        If typeS <> "" Then
            returnCode = 99
            status = -99
            Try
                returnCode = SCAPI.OpenReader(ComboBox1.Text, status)

                If returnCode = 0 Then
                    returnCode = 99
                    status = -99

                    Dim atr As String = Space(100)
                    Dim atr_len As Integer = 0
                    Dim timeOut As Integer = 100
                    Dim card_type As Integer = -999

                    returnCode = SCAPI.GetCardStatus(atr, atr_len, timeOut, card_type, status)

                    'TextBoxOutput.Text = TextBoxOutput.Text + Environment.NewLine + "Get Card Status [" + returnCode.ToString() + "] " + status.ToString()
                    'TextBoxOutput.Text = TextBoxOutput.Text + Environment.NewLine + "[ART] " + atr.Trim()

                    returnCode = 99
                    status = -99

                    Dim aid_bin(64) As Byte
                    Dim aid_bin_len As Integer
                    Dim util As New Utilities

                    util.Str2Bin(SCAPI.MOI_AID, aid_bin, aid_bin_len)
                    returnCode = SCAPI.SelectApplet(aid_bin(0), aid_bin_len, status)

                    If returnCode = 0 Then
                        returnCode = 99
                        status = -99

                        Dim block_id, offset As Integer
                        Dim dataBuf As String
                        Dim data_size As Integer

                        block_id = 0
                        offset = 4
                        data_size = 13
                        dataBuf = Space(15)

                        returnCode = SCAPI.ReadData(block_id, offset, data_size, dataBuf, status)
                        TextBoxPID = dataBuf

                        util.Str2Bin(SCAPI.ADM_AID, aid_bin, aid_bin_len)
                        returnCode = SCAPI.SelectApplet(aid_bin(0), aid_bin_len, status)

                        Dim cid, pre_perso, perso, chip, os As String

                        cid = Space(16)
                        pre_perso = Space(20)
                        perso = Space(20)
                        chip = Space(20)
                        os = Space(20)

                        returnCode = SCAPI.GetCardInfo(cid, chip, os, pre_perso, perso, status)
                        TextBoxCID = cid

                        If (TextBoxTKey = "") Then
                            send9080()
                            sendPinCode()
                            send9081()
                        End If

                        If typeS = "0101" Then
                            send0101()
                        ElseIf typeS = "0102" Then
                            send0102()
                        End If
                    Else
                        MessageBox.Show("[" + returnCode.ToString() + "] ")
                        'TextBoxOutput.Text = TextBoxOutput.Text + Environment.NewLine + "[" + returnCode.ToString() + "] " + status.ToString()
                    End If
                End If
            Catch ex As Exception
                Dim btn As Button = CType(sender, Button)
                MessageBox.Show(ex.Message,
                btn.Text,
                MessageBoxButtons.OK,
                MessageBoxIcon.Exclamation,
                MessageBoxDefaultButton.Button1)
            End Try
        End If
    End Sub

    Private Sub send9080()

        Dim recive_size As Integer
        Dim recive_max As Integer
        Dim timeOut As Integer = 1000
        returnCode = 99
        status = -99

        Try
            Dim send9080 As String '{0}{1}{2}{3} 9080 PID CID OfficeCode
            If LKOfficeCode.Trim().Length = 5 Then
                send9080 = String.Format("{0}{1}{2}{3}", "9080", TextBoxPID.Trim(), TextBoxCID.Trim(), LKOfficeCode.Trim()) '--Option :: Linkage Office Code 5 หลัก ของหน่วยงานผู้ร้องขอ
            Else
                send9080 = String.Format("{0}{1}{2}{3}", "9080", TextBoxPID.Trim(), TextBoxCID.Trim(), "     ") '--ใส่ช่องว่าง 5 ตัว
            End If
            recive_size = 0
            recive_max = 64 * 1024
            Dim rec_pointer As IntPtr = Marshal.AllocHGlobal(recive_max)
            returnCode = AMI_REQUEST("", send9080, send9080.Length, rec_pointer, recive_max, recive_size, timeOut, status) '--ส่งคำสั่ง AMI

            If returnCode <> 0 Then
                'TextBoxOutput.Text = TextBoxOutput.Text + Environment.NewLine + "[rc=" + returnCode + "] [st=" + status.ToString() + "]"
            Else
                Dim recive9080(recive_size) As Byte
                Marshal.Copy(rec_pointer, recive9080, 0, recive_size)
                rc9080 = New SCAPI.Recive9080(recive9080)
                If rc9080.ReturnStatus5 <> "00000" Then
                    'TextBoxOutput.Text = TextBoxOutput.Text + Environment.NewLine + Environment.NewLine + "[rs=" + rc9080.ReturnStatus5 + "]"
                Else
                    TextBoxKEY = rc9080.XKey32
                End If
            End If
        Catch ex As Exception
            'Dim btn As Button = CType(sender, Button)
        End Try
    End Sub


    Private Sub sendPinCode()
        Dim aid_bin(64) As Byte
        Dim aid_bin_len As Integer
        Dim util As New Utilities
        Try
            util.Str2Bin(SCAPI.ADM_AID, aid_bin, aid_bin_len)
            returnCode = SCAPI.SelectApplet(aid_bin(0), aid_bin_len, status)

            Dim adm_version, laser_number As String
            Dim authorize, adm_state As Integer

            adm_version = Space(5)
            adm_state = 0
            laser_number = Space(33)
            authorize = 0

            returnCode = 99
            status = -99
            returnCode = SCAPI.GetInfoADM(adm_version, adm_state, authorize, laser_number, status)

            If authorize = 0 Then
                'TextBoxOutput.Text = TextBoxOutput.Text + Environment.NewLine + "Cannot PIN Function"
            Else
                Dim try_remain As Integer

                returnCode = 99
                status = -99
                returnCode = VerifyPIN(1, 0, try_remain, status)

                If returnCode <> 0 Then
                    If status = 1001 Then
                        'TextBoxOutput.Text = TextBoxOutput.Text + Environment.NewLine + "PIN Incorrect try = " + try_remain.ToString()
                    End If
                Else
                    Dim req_mode, req_type, match_status, random_size, cryto_size As Integer
                    Dim random, cryto As String

                    req_mode = 0
                    req_type = 1
                    random = util.Bin2Str(TextBoxKEY, 32)
                    random_size = random.Length
                    cryto = Space(64)
                    cryto_size = 64
                    match_status = 0

                    returnCode = 99
                    status = -99
                    returnCode = GetMatchStatus(req_type, req_mode, random, random_size, cryto, cryto_size, match_status, status)

                    If returnCode <> 0 Then
                        'TextBoxOutput.Text = TextBoxOutput.Text + Environment.NewLine + "Get Match Status Error " + status.ToString()
                    Else
                        If match_status <> 1 Then
                            'TextBoxOutput.Text = TextBoxOutput.Text + Environment.NewLine + "Match Status not match " + match_status.ToString()
                        Else
                            TextBox4 = cryto

                            Dim envelop As String
                            Dim envelop_size As Integer

                            envelop_size = 255
                            envelop = Space(255)

                            returnCode = 99
                            status = -99
                            returnCode = EnvelopeGMSx(SCAPI.SAS_INT_AUTH_FPKEY_ADMIN, cryto, cryto_size, envelop, envelop_size, status) 'Key SAS_INT_AUTH_FPKEY_ADMIN

                            If returnCode <> 0 Then
                                'TextBoxOutput.Text = TextBoxOutput.Text + Environment.NewLine + "Envelop not work"
                            Else
                                TextBox5 = envelop
                            End If
                        End If
                    End If
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub send9081()

        Dim reply_size As Integer
        Dim reply_max As Integer
        Dim timeOut As Integer = 1000

        Try
            '{0}{1}{2}{3}{4}:{5} --> 9081 PID CID XKey EnVelopGMSx.size : EnVelopGMSx  
            Dim send9081 As String = String.Format("{0}{1}{2}{3}{4}:{5}", "9081", TextBoxPID.Trim(), TextBoxCID.Trim(), TextBoxKEY.Trim(), TextBox5.Trim().Length.ToString(), TextBox5.Trim())

            reply_size = 0
            reply_max = 64 * 1024
            Dim rec_pointer As IntPtr = Marshal.AllocHGlobal(reply_max)

            returnCode = 99
            status = -99
            returnCode = AMI_REQUEST("", send9081, send9081.Length, rec_pointer, reply_max, reply_size, timeOut, status)

            If returnCode <> 0 Then
                'TextBoxOutput.Text = TextBoxOutput.Text + Environment.NewLine + "[rc=" + status.ToString() + "]"
            Else
                Dim recive9081(reply_size) As Byte
                Marshal.Copy(rec_pointer, recive9081, 0, reply_size)
                rc9081 = New SCAPI.Recive9081(recive9081)
                TextBoxTKey = rc9081.TKey32
                If rc9081.ReturnStatus5 <> "00000" Then
                    'TextBoxOutput.Text = TextBoxOutput.Text + Environment.NewLine + "[rs=" + rc9081.ReturnStatus5 + "]"
                Else
                    'TextBoxOutput.Text = TextBoxOutput.Text + Environment.NewLine + "Authorize LOGIN Success"
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub


    Private Sub send0101()
        Try

            Dim send_data As New GetPOP
            Dim reply_data As New ReplyPOP

            Dim send_datasize As Integer
            Dim reply_datasize As Integer
            Dim reply_datamax As Integer
            Dim timeOut As Integer
            Dim status As Integer


            reply_datasize = Marshal.SizeOf(reply_data)
            reply_datamax = Marshal.SizeOf(reply_data)

            send_data.Reqno = Encoding.Default.GetBytes("0101")
            send_data.ReqId = Encoding.Default.GetBytes(New String(" ", 9))
            send_data.ReqPw = Encoding.Default.GetBytes(New String(" ", 4))

            Dim target As String
            target = TextBoxPIDex.Text.Trim()

            target = target & New String(" ", 48 - target.Length)
            send_data.ReqKey = Encoding.Default.GetBytes(target)

            send_data.ReplyCode = Encoding.Default.GetBytes("0")
            send_data.ReqLevel = Encoding.Default.GetBytes("1")
            Dim af_the_star As String = New String("1", 61)
            send_data.ActiveField = Encoding.Default.GetBytes(af_the_star)

            send_data.ReqPID = Encoding.Default.GetBytes(TextBoxPID.Trim())
            send_data.ReqCID = Encoding.Default.GetBytes(TextBoxCID.Trim())
            send_data.ReqXXX = Encoding.Default.GetBytes(TextBoxKEY.Trim())

            send_datasize = Marshal.SizeOf(send_data)
            timeOut = 50

            returnCode = 99
            status = -99
            returnCode = AMI_REQUEST("", send_data, send_datasize, reply_data, reply_datamax, reply_datasize, timeOut, status)

            If byte2str(reply_data.returnCode) = "00000" Then
                RStatus.Text = "AMI Status = " & status & "   " & "Base Status = " & byte2str(reply_data.returnCode)
                PID.Text = byte2str(reply_data.PID).Trim()
                FullName.Text = byte2str(reply_data.Title).Trim() + byte2str(reply_data.FNAME).Trim() + " " + byte2str(reply_data.LNAME).Trim()
                HDesc.Text = byte2str(reply_data.HDesc).Trim().Replace("#", " ").Replace("00", " ").Replace("  ", " ")

                Dim DobTxt = byte2str(reply_data.Dob).Trim()
                Dob.Text = Mid(DobTxt, 7, 2) & " " & GenMonthThai(Mid(DobTxt, 5, 2), 2) & " " & Microsoft.VisualBasic.Left(DobTxt, 4)

                FPID.Text = byte2str(reply_data.FPID).Trim()
                MPID.Text = byte2str(reply_data.MPID).Trim()
                FNat.Text = byte2str(reply_data.FNat).Trim()
                MNat.Text = byte2str(reply_data.MNat).Trim()
                Nat.Text = byte2str(reply_data.Nat).Trim()
                Hid.Text = byte2str(reply_data.Hid).Trim()
                Sex2.Text = byte2str(reply_data.Sex).Trim()
                MFName.Text = byte2str(reply_data.MFName).Trim()
                FFName.Text = byte2str(reply_data.FFName).Trim()

                DMoveIn.Text = Encoding.Default.GetString(reply_data.DMoveIn).Trim()
                HStat.Text = byte2str(reply_data.HStat).Trim()
                PStat.Text = byte2str(reply_data.PStat).Trim()
                DMoveIn.Text = byte2str(reply_data.DMoveIn).Trim()


                'Reqno.Text = Encoding.Default.GetString(reply_data.Reqno).Trim()
                'ActiveField.Text = Encoding.Default.GetString(reply_data.ActiveField).Trim()
                'returnCode.Text = Encoding.Default.GetString(reply_data.returnCode).Trim()
                'Reserve.Text = Encoding.Default.GetString(reply_data.Reserve).Trim()
                'age.Text = Encoding.Default.GetString(reply_data.age).Trim()
                'ChangeNat.Text = Encoding.Default.GetString(reply_data.ChangeNat).Trim()
                'DChangeNat.Text = Encoding.Default.GetString(reply_data.DChangeNat).Trim()

                ToolStripButton1.Enabled = True
                ToolStripComboBox1.Enabled = True
            Else
                RStatus.ForeColor = System.Drawing.ColorTranslator.FromOle(RGB(255, 0, 0))
                Dim err_str As String = byte2str(reply_data.returnCode)
                ShowErrIkno(err_str)
                RStatus.Text = "AMI Status = " & status & "   " & "Base Status = " & err_str
            End If
        Catch ex As Exception
        End Try
    End Sub

    Public Function str2byte(ByVal strIn As String) As Byte()
        str2byte = System.Text.Encoding.Default.GetBytes(strIn)
    End Function

    Public Sub send0102()
        Dim send_data As GetPOP

        Dim reply_data As ReplyPOP
        Dim ReturnValue As Short
        Dim send_datasize As Integer
        Dim reply_datasize As Integer
        Dim reply_datamax As Integer
        Dim timeOut As Integer
        Dim status As Integer

        '    txtPid.Text = ""
        '    FLNAME.Text = ""
        '    Detail.Text = ""
        '    Address.Text = ""
        '    out = ""

        reply_datasize = Marshal.SizeOf(reply_data)
        reply_datamax = Marshal.SizeOf(reply_data)

        send_data.Reqno = str2byte("0102")
        send_data.ReqId = str2byte(New String(" ", 9))
        send_data.ReqPw = str2byte(New String(" ", 4))

        Dim target As String
        target = FNAME.Text & New String(" ", 24 - FNAME.Text.Length)
        target = target & LNAME.Text & New String(" ", 24 - LNAME.Text.Length)
        send_data.ReqKey = str2byte(target)

        send_data.ReplyCode = str2byte("0")
        send_data.ReqLevel = str2byte("1")
        Dim af_the_star As String = New String("1", 61)
        send_data.ActiveField = str2byte(af_the_star)

        send_data.ReqPID = str2byte(TextBoxPID.Trim())
        send_data.ReqCID = str2byte(TextBoxCID.Trim())
        send_data.ReqXXX = str2byte(TextBoxKEY.Trim())

        send_datasize = Marshal.SizeOf(send_data)
        timeOut = 50

        ReturnValue = AMI_REQUEST("",
                        send_data, send_datasize,
                        reply_data, reply_datamax, reply_datasize,
                        timeOut, status)

        If byte2str(reply_data.returnCode) = "00000" Then
            RStatus.Text = "AMI Status = " & status & "   " & "Base Status = " & byte2str(reply_data.returnCode)
            PID.Text = byte2str(reply_data.PID).Trim()
            FullName.Text = byte2str(reply_data.Title).Trim() + byte2str(reply_data.FNAME).Trim() + " " + byte2str(reply_data.LNAME).Trim()
            HDesc.Text = byte2str(reply_data.HDesc).Trim().Replace("#", " ").Replace("  ", " ")

            Dim DobTxt = byte2str(reply_data.Dob).Trim()
            Dob.Text = Mid(DobTxt, 7, 2) & " " & GenMonthThai(Mid(DobTxt, 5, 2), 2) & " " & Microsoft.VisualBasic.Left(DobTxt, 4)

            FPID.Text = byte2str(reply_data.FPID).Trim()
            MPID.Text = byte2str(reply_data.MPID).Trim()
            FNat.Text = byte2str(reply_data.FNat).Trim()
            MNat.Text = byte2str(reply_data.MNat).Trim()
            Nat.Text = byte2str(reply_data.Nat).Trim()
            Hid.Text = byte2str(reply_data.Hid).Trim()
            Sex.Text = byte2str(reply_data.Sex).Trim()
            MFName.Text = byte2str(reply_data.MFName).Trim()
            FFName.Text = byte2str(reply_data.FFName).Trim()

            HStat.Text = byte2str(reply_data.HStat).Trim()
            PStat.Text = byte2str(reply_data.PStat).Trim()
            DMoveIn.Text = byte2str(reply_data.DMoveIn).Trim()


            'Reqno.Text = Encoding.Default.GetString(reply_data.Reqno).Trim()
            'ActiveField.Text = Encoding.Default.GetString(reply_data.ActiveField).Trim()
            'returnCode.Text = Encoding.Default.GetString(reply_data.returnCode).Trim()
            'Reserve.Text = Encoding.Default.GetString(reply_data.Reserve).Trim()

            'age.Text = Encoding.Default.GetString(reply_data.age).Trim()
            'ChangeNat.Text = Encoding.Default.GetString(reply_data.ChangeNat).Trim()
            'DChangeNat.Text = Encoding.Default.GetString(reply_data.DChangeNat).Trim()


            ToolStripButton1.Enabled = True
            ToolStripComboBox1.Enabled = True

            btnBack.Visible = True
            btnNext.Visible = True
        Else
            RStatus.ForeColor = System.Drawing.ColorTranslator.FromOle(RGB(255, 0, 0))
            Dim err_str As String = byte2str(reply_data.returnCode)
            ShowErrIkno(err_str)
            RStatus.Text = "AMI Status = " & status & "   " & "Base Status = " & err_str
        End If

    End Sub

    Public Sub sendActionPerson(ByVal code As String)
        Dim send_data As GetPOP

        Dim reply_data As ReplyPOP
        Dim ReturnValue As Short
        Dim send_datasize As Integer
        Dim reply_datasize As Integer
        Dim reply_datamax As Integer
        Dim timeOut As Integer
        Dim status As Integer

        '    txtPid.Text = ""
        '    FLNAME.Text = ""
        '    Detail.Text = ""
        '    Address.Text = ""
        '    out = ""

        reply_datasize = Marshal.SizeOf(reply_data)
        reply_datamax = Marshal.SizeOf(reply_data)

        send_data.Reqno = str2byte(code)
        send_data.ReqId = str2byte(New String(" ", 9))
        send_data.ReqPw = str2byte(New String(" ", 4))

        Dim target As String
        target = FNAME.Text & New String(" ", 24 - FNAME.Text.Length)
        target = target & LNAME.Text & New String(" ", 24 - LNAME.Text.Length)
        send_data.ReqKey = str2byte(target)

        send_data.ReplyCode = str2byte("0")
        send_data.ReqLevel = str2byte("1")
        Dim af_the_star As String = New String("1", 61)
        send_data.ActiveField = str2byte(af_the_star)

        send_data.ReqPID = str2byte(TextBoxPID.Trim())
        send_data.ReqCID = str2byte(TextBoxCID.Trim())
        send_data.ReqXXX = str2byte(TextBoxKEY.Trim())

        send_datasize = Marshal.SizeOf(send_data)
        timeOut = 50

        ReturnValue = AMI_REQUEST("",
                        send_data, send_datasize,
                        reply_data, reply_datamax, reply_datasize,
                        timeOut, status)

        If byte2str(reply_data.returnCode) = "00000" Then
            RStatus.Text = "AMI Status = " & status & "   " & "Base Status = " & byte2str(reply_data.returnCode)
            PID.Text = byte2str(reply_data.PID).Trim()
            FullName.Text = byte2str(reply_data.Title).Trim() + byte2str(reply_data.FNAME).Trim() + " " + byte2str(reply_data.LNAME).Trim()
            HDesc.Text = byte2str(reply_data.HDesc).Trim().Replace("#", " ").Replace("  ", " ")

            Dim DobTxt = byte2str(reply_data.Dob).Trim()
            Dob.Text = Mid(DobTxt, 7, 2) & " " & GenMonthThai(Mid(DobTxt, 5, 2), 2) & " " & Microsoft.VisualBasic.Left(DobTxt, 4)

            FPID.Text = byte2str(reply_data.FPID).Trim()
            MPID.Text = byte2str(reply_data.MPID).Trim()
            FNat.Text = byte2str(reply_data.FNat).Trim()
            MNat.Text = byte2str(reply_data.MNat).Trim()
            Nat.Text = byte2str(reply_data.Nat).Trim()
            Hid.Text = byte2str(reply_data.Hid).Trim()
            Sex.Text = byte2str(reply_data.Sex).Trim()
            MFName.Text = byte2str(reply_data.MFName).Trim()
            FFName.Text = byte2str(reply_data.FFName).Trim()

            HStat.Text = byte2str(reply_data.HStat).Trim()
            PStat.Text = byte2str(reply_data.PStat).Trim()
            DMoveIn.Text = byte2str(reply_data.DMoveIn).Trim()


            'Reqno.Text = Encoding.Default.GetString(reply_data.Reqno).Trim()
            'ActiveField.Text = Encoding.Default.GetString(reply_data.ActiveField).Trim()
            'returnCode.Text = Encoding.Default.GetString(reply_data.returnCode).Trim()
            'Reserve.Text = Encoding.Default.GetString(reply_data.Reserve).Trim()

            'age.Text = Encoding.Default.GetString(reply_data.age).Trim()
            'ChangeNat.Text = Encoding.Default.GetString(reply_data.ChangeNat).Trim()
            'DChangeNat.Text = Encoding.Default.GetString(reply_data.DChangeNat).Trim()


            ToolStripButton1.Enabled = True
            ToolStripComboBox1.Enabled = True
        Else
            RStatus.ForeColor = System.Drawing.ColorTranslator.FromOle(RGB(255, 0, 0))
            Dim err_str As String = byte2str(reply_data.returnCode)
            ShowErrIkno(err_str)
            RStatus.Text = "AMI Status = " & status & "   " & "Base Status = " & err_str
        End If

    End Sub
    Sub ShowErrIkno(ByRef ErrCode As String)
        Dim ShowMsg As Object
        Dim flagCase As Object
        Dim sTitle As String
        Dim sErrMsg As String
        Dim bStyle As Byte
        Dim iTmp As Short
        'Dim iMouse As Short
        Dim Count As Short
        'Dim stCode As String
        'UPGRADE_WARNING: Screen property Screen.MousePointer has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
        'iMouse = System.Windows.Forms.Cursor.Current
        'UPGRADE_WARNING: Screen property Screen.MousePointer has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
        sTitle = "รายงานผลความผิดพลาดของระบบ"
        bStyle = 1
        sErrMsg = ""
        Count = Len(Trim(ErrCode))
        If Count = 5 Then 'Return code from Server
            Select Case Trim(ErrCode)
                Case "00001" : sErrMsg = "ไม่มี Record ถัดไปให้อ่าน"
                Case "00815" : sErrMsg = "ไม่พบรายการในฐานข้อมูล"
                Case "00870"
                    'UPGRADE_WARNING: Couldn't resolve default property of object flagCase. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If flagCase = 1 Then
                        sErrMsg = "ไม่พบรายการซ้ำถัดไปในฐานข้อมูล"
                        'UPGRADE_WARNING: Couldn't resolve default property of object flagCase. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    ElseIf flagCase = 2 Then
                        sErrMsg = "ไม่พบรายการซ้ำก่อนหน้าในฐานข้อมูล"
                    End If
                Case "90001" : sErrMsg = "หมายเลขประจำตัวไม่ถูกต้อง"
                Case "90002" : sErrMsg = "รหัสลับไม่ถูกต้อง"
                Case "90003" : sErrMsg = "รหัสลับหมดอายุการใช้งาน"
                Case "90005" : sErrMsg = "ไม่อนุญาตให้ติดต่อกับ Server ในเวลานี้"
                Case "90006" : sErrMsg = "Secret code out of length"
                Case "90007" : sErrMsg = "Invalid secret code"
                Case "90008" : sErrMsg = "ใช้สิทธิในการตรวจสอบข้อมูลครบแล้ว" '
                    'Case "90009": sErrMsg = "ไม่มีสิทธิในการทำงาน (not found in card_ctl)"  '
                Case "90009" : sErrMsg = "ไม่พบข้อมูลบัตร Smart Card" '
                Case "90010" : sErrMsg = "ไม่สามารถติดต่อ Server ที่ใช้ในการค้นหาข้อมูลได้"
                Case "90011" : sErrMsg = "ข้อมูลที่ส่งมาตรวจสอบไม่ถูกต้อง" '
                Case "90028" : sErrMsg = "ไม่พบข้อมูลผู้ได้รับอนุญาตในระบบ" '
                Case "90031" : sErrMsg = "ไม่สามารถทำงานได้เนื่องจากติดต่อขอรหัสการเข้าใช้งานไม่ได้" '
                Case "90032" : sErrMsg = "ไม่สามารถทำงานได้เนื่องจากตรวจสอบรหัสการเข้าใช้งานไม่ได้" '
                    'Case "90040": sErrMsg = "ไม่มีสิทธิในการทำงาน (card_st error)"  ' by First 31/03/53
                Case "90040" : sErrMsg = "บัตรไม่ได้อยู่ในสถานะปกติ" ' by First 31/03/53
                Case "90041" : sErrMsg = "ไม่มีสิทธิในการทำงาน (Update Using Code in emp_card error)" '
                Case "90042" : sErrMsg = "ไม่มีสิทธิในการทำงาน (Not found in emp_card)" '
                Case "90043" : sErrMsg = "ไม่มีสิทธิในการทำงาน (Using Code not match in emp_card)" ' by First 31/03/53
                Case "90044" : sErrMsg = "ไม่มีสิทธิในการทำงาน (Check SAS error)" ' by First 31/03/53
                Case "90045" : sErrMsg = "ไม่มีสิทธิในการทำงาน (SAS error - not match)" ' by First 31/03/53
                Case "90046" : sErrMsg = "ไม่มีสิทธิในการทำงาน (Using Code sened not match in emp_card)" ' by First 31/03/53
                Case "99701" : sErrMsg = "ไม่พบรายการการเปลี่ยนแปลงที่อยู่ในฐานข้อมูล"
                Case "99702" : sErrMsg = "ไม่พบรายการการเปลี่ยนแปลงชื่อในฐานข้อมูล"
                Case "99703" : sErrMsg = "ไม่พบรายการการเปลี่ยนแปลง สัญชาติในฐานข้อมูล"
                Case "99704" : sErrMsg = "ไม่มีสิทธิตรวจสอบรายการการเปลี่ยนแปลง ที่อยู่ในฐานข้อมูล"
                Case "99705" : sErrMsg = "ไม่มีสิทธิตรวจสอบรายการการเปลี่ยนแปลง ชื่อในฐานข้อมูล"
                Case "99706" : sErrMsg = "ไม่พบรายการชื่อกลางในฐานข้อมูล"
                Case "99707" : sErrMsg = "ไม่พบรายการชื่อภาษาอังกฤษในฐานข้อมูล"
                Case "99708" : sErrMsg = "ไม่สามารถตรวจสอบรูปภาพลายนิ้วมือได้"
                Case "99709" : sErrMsg = "ไม่พบข้อมูลรูปภาพลายนิ้วมือในฐานข้อมูล"
                Case "99801" : sErrMsg = "ไม่ระบุค่าของบ้านเลขที่และรหัสจังหวัด อำเภอ ตำบล"
                Case "99983" : sErrMsg = "ไม่สามารถส่งภาพใบหน้าได้ เนื่องจากมีขนาดเกิน 20 KB"
                Case "99984" : sErrMsg = "การติดต่อการค้นหารายการ บัตร/ภาพใบหน้ามีปัญหา"
                Case "99985" : sErrMsg = "ไม่สามารถตรวจสอบบ้าน/บุคคลรายการก่อนหน้า หรือรายการถัดไปได้ เนื่องจากหมดเวลาในการใช้ข้อมูลได้"
                Case "99986" : sErrMsg = "การติดต่อการค้นหารายการบ้าน/บุคคล กรณีระบุเงื่อนไขมีปัญหา"
                Case "99987" : sErrMsg = "ไม่สามารถตรวจสอบรายการบ้าน/บุคคล กรณีระบุเงื่อนไขได้"
                Case "99988" : sErrMsg = "ไม่สามารถตรวจสอบรายการบ้าน/บุคคลได้"
                Case "99989" : sErrMsg = "ไม่สามารถตรวจสอบรายการบัตรได้"
                Case "99990" : sErrMsg = "เลขควบคุม 2 ไม่ถูกต้อง"
                Case "99991"
                    'UPGRADE_WARNING: Couldn't resolve default property of object flagCase. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If flagCase = 1 Then
                        sErrMsg = "ไม่พบรายการบัตรถัดไปในฐานข้อมูล"
                        'UPGRADE_WARNING: Couldn't resolve default property of object flagCase. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    ElseIf flagCase = 2 Then
                        sErrMsg = "ไม่พบรายการบัตรก่อนหน้าในฐานข้อมูล"
                    End If
                Case "99992" : sErrMsg = "ไม่พบรายการบัตรในฐานข้อมูล"
                Case "99993" : sErrMsg = "ไม่สามารถอ่านไฟล์ภาพใบหน้าได้"
                Case "99994" : sErrMsg = "ไม่พบภาพใบหน้าในฐานข้อมูล"
                Case "99995" : sErrMsg = "ไม่สามารถตรวจสอบภาพใบหน้าได้"
                Case "99996" : sErrMsg = "รหัสผ่านข้อมูลประจำตัวไม่ถูกต้อง"
                Case "99997" : sErrMsg = "ไม่สามารถตรวจสอบเลขควบคุม บัตรประจำตัวประชาชนได้"
                Case "99998" : sErrMsg = "เลขควบคุมบัตรประจำตัวประชาชนไม่ถูกต้อง"
                Case "99999" : sErrMsg = "ไม่มีสิทธิในการทำงาน"
                Case Else : sErrMsg = "[" & Trim(ErrCode) & "]: เกิดความผิดพลาดกรณีอื่น ๆ"
            End Select
        ElseIf Count = 3 Then  ' AMI Error
            Select Case Trim(ErrCode)
                Case "102" : sErrMsg = "[" & Trim(ErrCode) & "] CONNECT_LOST"
                Case "101" : sErrMsg = "[" & Trim(ErrCode) & "] REQUEST_TIMEOUT"
                Case "103" : sErrMsg = "[" & Trim(ErrCode) & "] CANT_START_SERVER"
                Case "105" : sErrMsg = "[" & Trim(ErrCode) & "] CONNECTION_FULL"
                Case "107" : sErrMsg = "[" & Trim(ErrCode) & "] CANT_CONNECT"
                Case "109" : sErrMsg = "[" & Trim(ErrCode) & "] NO_REQUEST_CONNECTION"
                Case "111" : sErrMsg = "[" & Trim(ErrCode) & "] SEND_ERROR"
                Case "113" : sErrMsg = "[" & Trim(ErrCode) & "] RECV_ERROR"
                Case "115" : sErrMsg = "[" & Trim(ErrCode) & "] MESSAGE_TOO_LARGE"
                Case "201" : sErrMsg = "[" & Trim(ErrCode) & "] UNKNOWN_MODE"
                Case "203" : sErrMsg = "[" & Trim(ErrCode) & "] WRONG_MODE"
                Case Else : sErrMsg = "[" & Trim(ErrCode) & "]: เกิดความผิดพลาดกรณีอื่น ๆ"
            End Select
        End If

        ErrCode = sErrMsg

    End Sub

    Public Function byte2str(ByVal byteIn As Byte()) As String
        byte2str = System.Text.Encoding.Default.GetString(byteIn)
    End Function
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles ToolStripButton1.Click
        PrintDocument1.Print()
        If ToolStripComboBox1.SelectedIndex = 0 Then
            Panel1.BackColor = System.Drawing.Color.Transparent
            Panel1.BorderStyle = System.Windows.Forms.BorderStyle.None
        ElseIf ToolStripComboBox1.SelectedIndex = 1 Then
            Panel3.BackColor = System.Drawing.Color.Transparent
            Panel3.BorderStyle = System.Windows.Forms.BorderStyle.None
        ElseIf ToolStripComboBox1.SelectedIndex = 2 Then
            Panel1.BackColor = System.Drawing.Color.Transparent
            Panel1.BorderStyle = System.Windows.Forms.BorderStyle.None

            Panel3.BackColor = System.Drawing.Color.Transparent
            Panel3.BorderStyle = System.Windows.Forms.BorderStyle.None
        End If

    End Sub

    Private Sub checkRadio()
        btnBack.Visible = False
        btnNext.Visible = False
        If RadioButton2.Checked Then
            TextBoxPIDex.Text = ""
            TextBoxPIDex.Enabled = True
            FNAME.Enabled = False
            LNAME.Enabled = False
            FNAME.Text = ""
            LNAME.Text = ""
        Else
            TextBoxPIDex.Text = ""
            TextBoxPIDex.Enabled = False
            FNAME.Enabled = True
            LNAME.Enabled = True
            FNAME.Text = ""
            LNAME.Text = ""
        End If
    End Sub

    Private Sub clearData()
        TextBoxPIDex.Text = ""
        PID.Text = ""
        PID2.Text = ""
        FullName2.Text = ""
        FullName.Text = ""
        HDesc.Text = ""
        HDesc2.Text = ""
        Dob2.Text = ""
        Dob.Text = ""
        DobEn2.Text = ""
        DMoveIn.Text = ""
        'Reqno.Text = ""
        'ActiveField.Text = ""
        Nat.Text = ""
        'returnCode.Text = ""
        'Reserve.Text = ""
        HStat.Text = ""
        PStat.Text = ""
        'age.Text = ""
        FPID.Text = ""
        MPID.Text = ""
        FNat.Text = ""
        MNat.Text = ""
        'ChangeNat.Text = ""
        'DChangeNat.Text = ""
        Hid.Text = ""
        Sex2.Text = ""
        Sex.Text = ""
        MFName.Text = ""
        FFName.Text = ""

        IssuedDate.Text = ""
        ExpireDate.Text = ""
        IssuedDateEn.Text = ""
        ExpireDateEn.Text = ""

        FullNameEn.Text = ""
        place.Text = ""
        dateActive.Text = ""
        SexEn.Text = ""
        idCard.Text = ""
        RStatus.Text = ""
        namePic = ""

        checkRadio()




        PictureBox1.Image = Global.TESTAMI.My.Resources.Resources.Antu_user
        'ToolStripButton1.Enabled = False
        'ToolStripComboBox1.Enabled = False
        ToolStripComboBox1.SelectedIndex = 0


    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim newMargins As System.Drawing.Printing.Margins
        newMargins = New System.Drawing.Printing.Margins(0, 0, 0, 0)
        PrintDocument1.DefaultPageSettings.Margins = newMargins

        Dim WidthPanel As Integer
        Dim HeightPanel As Integer
        Dim b As Bitmap

        If ToolStripComboBox1.SelectedIndex = 0 Then
            Panel1.BackColor = System.Drawing.Color.White
            Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle

            WidthPanel = Panel1.Width
            HeightPanel = Panel1.Height

            b = New Bitmap(WidthPanel, HeightPanel)

            Dim PanA As Rectangle = New Rectangle(0, 0, WidthPanel, HeightPanel)
            Panel1.DrawToBitmap(b, PanA)

        ElseIf ToolStripComboBox1.SelectedIndex = 1 Then
            Panel3.BackColor = System.Drawing.Color.White
            Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle

            WidthPanel = Panel3.Width
            HeightPanel = Panel3.Height

            b = New Bitmap(WidthPanel, HeightPanel)

            Dim PanA As Rectangle = New Rectangle(0, 0, WidthPanel, HeightPanel)
            Panel3.DrawToBitmap(b, PanA)

        ElseIf ToolStripComboBox1.SelectedIndex = 2 Then
            Panel1.BackColor = System.Drawing.Color.White
            Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle

            Panel3.BackColor = System.Drawing.Color.White
            Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle

            WidthPanel = Panel1.Width
            HeightPanel = Panel1.Height

            WidthPanel += Panel3.Width
            HeightPanel += Panel3.Height

            b = New Bitmap(WidthPanel, HeightPanel)

            Dim PanA As Rectangle = New Rectangle(0, 0, Panel1.Width, Panel1.Height)
            Dim PanB As Rectangle = New Rectangle(0, Panel1.Height, Panel3.Width, Panel3.Height)
            Panel1.DrawToBitmap(b, PanA)
            Panel1.DrawToBitmap(b, PanB)




        End If




        e.Graphics.CompositingQuality = Drawing2D.CompositingQuality.HighQuality
        e.Graphics.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
        e.Graphics.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
        e.Graphics.Clear(Color.White)
        e.Graphics.DrawImage(b, New Point(90, 50))
    End Sub


    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        clearData()
        listReader()
    End Sub


    Private Sub checKProcessIM()
        Try
            Dim app_exe As String = "lm.exe"
            Dim Process As Object
            For Each Process In GetObject("winmgmts:").ExecQuery("Select Name from Win32_Process Where Name = '" & app_exe & "'")
                Process.Terminate
            Next
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Frm_AMI_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        checKProcessIM()
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click

        clearData()
        returnCode = 99
        status = -99
        Try
            returnCode = SCAPI.OpenReader(ComboBox1.Text, status)

            If returnCode = 0 Then
                returnCode = 99
                status = -99

                Dim atr As String = Space(100)
                Dim atr_len As Integer = 0
                Dim timeOut As Integer = 100
                Dim card_type As Integer = -999

                returnCode = SCAPI.GetCardStatus(atr, atr_len, timeOut, card_type, status)



                returnCode = 99
                status = -99

                Dim aid_bin(64) As Byte
                Dim aid_bin_len As Integer
                Dim util As New Utilities

                util.Str2Bin(SCAPI.MOI_AID, aid_bin, aid_bin_len)
                returnCode = SCAPI.SelectApplet(aid_bin(0), aid_bin_len, status)

                If returnCode = 0 Then
                    returnCode = 99
                    status = -99

                    Dim block_id, offset As Integer
                    Dim dataBuf As String
                    Dim data_size As Integer

                    block_id = 0
                    offset = 0
                    data_size = 5927
                    dataBuf = Space(5930)

                    Dim Vers As String = ""
                    Dim PID As String = ""
                    Dim FullName As String = ""
                    Dim FullNameE As String = ""
                    Dim Dob As String = ""
                    Dim DobEn As String = ""
                    Dim Sex As String = ""
                    Dim Number As String = ""
                    Dim IssuedAt As String = ""
                    Dim IssuedDateT As String = ""
                    Dim ExpireDateT As String = ""
                    Dim IssuedDateE As String = ""
                    Dim ExpireDateE As String = ""
                    Dim HDesc As String = ""
                    Dim placeT As String = ""

                    returnCode = SCAPI.ReadData(block_id, offset, data_size, dataBuf, status)

                    Vers = dataBuf.Substring(0, 4)
                    If Vers = "0002" Then
                        PID = dataBuf.Substring(4, 13)
                        FullName = dataBuf.Substring(17, 100).Trim().Replace("#", " ")
                        FullNameE = dataBuf.Substring(117, 100).Trim().Replace("#", " ")
                        Dim DobTxt As String = dataBuf.Substring(217, 8)
                        Dob = Mid(DobTxt, 7, 2) & " " & GenMonthThai(Mid(DobTxt, 5, 2), 2) & " " & Microsoft.VisualBasic.Left(DobTxt, 4)
                        DobEn = Mid(DobTxt, 7, 2) & " " & GenMonthEn(Mid(DobTxt, 5, 2), 2) & " " & (Convert.ToInt32(Microsoft.VisualBasic.Left(DobTxt, 4)) - 543)
                        Dim Gender As String = dataBuf.Substring(225, 1)
                        If Gender = "1" Then
                            Sex = "ชาย"
                            SexEn.Text = "Male"
                        Else
                            Sex = "หญิง"
                            SexEn.Text = "Female"
                        End If
                        Number = dataBuf.Substring(226, 20)
                        IssuedAt = dataBuf.Substring(246, 100)
                        Dim IssuedDateTxt As String = dataBuf.Substring(359, 8)
                        IssuedDateT = Mid(IssuedDateTxt, 7, 2) & " " & GenMonthThai(Mid(IssuedDateTxt, 5, 2), 2) & " " & Microsoft.VisualBasic.Left(IssuedDateTxt, 4)
                        IssuedDateE = Mid(IssuedDateTxt, 7, 2) & " " & GenMonthEn(Mid(IssuedDateTxt, 5, 2), 2) & " " & (Convert.ToInt32(Microsoft.VisualBasic.Left(IssuedDateTxt, 4)) - 543)
                        Dim ExpireDateTxt As String = dataBuf.Substring(367, 8)
                        ExpireDateT = Mid(ExpireDateTxt, 7, 2) & " " & GenMonthThai(Mid(ExpireDateTxt, 5, 2), 2) & " " & Microsoft.VisualBasic.Left(ExpireDateTxt, 4)
                        ExpireDateE = Mid(ExpireDateTxt, 7, 2) & " " & GenMonthEn(Mid(ExpireDateTxt, 5, 2), 2) & " " & (Convert.ToInt32(Microsoft.VisualBasic.Left(ExpireDateTxt, 4)) - 543)

                        placeT = dataBuf.Substring(246, 100).Replace("#", " ").Replace("  ", " ")

                        Dim ImageSize As Integer
                        Dim image As String
                        Dim imagebuf As String
                        Dim fileNum As Integer

                        imagebuf = Mid(dataBuf, 378, 5120)

                        ImageSize = Asc(Mid(Trim(imagebuf), 1, 1)) * 256 + Asc(Mid(Trim(imagebuf), 2, 1))
                        image = Space(ImageSize)
                        image = Mid(imagebuf, 3, ImageSize)
                        fileNum = FreeFile()
                        FileOpen(fileNum, "Image.jpg", OpenMode.Binary, OpenAccess.Write)
                        FilePut(fileNum, image)
                        FileClose(fileNum)
                        PictureBox1.Image = System.Drawing.Image.FromFile("Image.jpg")
                        HDesc = Mid(dataBuf, 5497, 160).Replace("#", " ").Replace("  ", " ")

                        idCard.Text = Mid(dataBuf, 5657, 14)
                    Else
                        PID = dataBuf.Substring(4, 13)
                        FullName = dataBuf.Substring(17, 100).Trim().Replace("#", " ")
                        FullNameE = dataBuf.Substring(117, 100).Trim().Replace("#", " ")
                        Dim DobTxt As String = dataBuf.Substring(217, 8)
                        Dob = Mid(DobTxt, 7, 2) & " " & GenMonthThai(Mid(DobTxt, 5, 2), 2) & " " & Microsoft.VisualBasic.Left(DobTxt, 4)
                        DobEn = Mid(DobTxt, 7, 2) & " " & GenMonthEn(Mid(DobTxt, 5, 2), 2) & " " & (Convert.ToInt32(Microsoft.VisualBasic.Left(DobTxt, 4)) - 543)
                        Dim Gender As String = dataBuf.Substring(225, 1)
                        If Gender = "1" Then
                            Sex = "ชาย"
                            SexEn.Text = "Male"
                        Else
                            Sex = "หญิง"
                            SexEn.Text = "Female"
                        End If
                        Number = dataBuf.Substring(226, 20)
                        IssuedAt = dataBuf.Substring(246, 100)
                        Dim IssuedDateTxt As String = dataBuf.Substring(359, 8)
                        IssuedDateT = Mid(IssuedDateTxt, 7, 2) & " " & GenMonthThai(Mid(IssuedDateTxt, 5, 2), 2) & " " & Microsoft.VisualBasic.Left(IssuedDateTxt, 4)
                        IssuedDateE = Mid(IssuedDateTxt, 7, 2) & " " & GenMonthEn(Mid(IssuedDateTxt, 5, 2), 2) & " " & (Convert.ToInt32(Microsoft.VisualBasic.Left(IssuedDateTxt, 4)) - 543)
                        Dim ExpireDateTxt As String = dataBuf.Substring(367, 8)
                        ExpireDateT = Mid(ExpireDateTxt, 7, 2) & " " & GenMonthThai(Mid(ExpireDateTxt, 5, 2), 2) & " " & Microsoft.VisualBasic.Left(ExpireDateTxt, 4)
                        ExpireDateE = Mid(ExpireDateTxt, 7, 2) & " " & GenMonthEn(Mid(ExpireDateTxt, 5, 2), 2) & " " & (Convert.ToInt32(Microsoft.VisualBasic.Left(ExpireDateTxt, 4)) - 543)

                        placeT = dataBuf.Substring(246, 100).Replace("#", " ").Replace("  ", " ")



                        Dim ImageSize As Integer
                        Dim image As String
                        Dim imagebuf As String
                        Dim fileNum As Integer

                        imagebuf = Mid(dataBuf, 378, 5120)

                        Dim fileName As String = PID & ".jpg"

                        ImageSize = Asc(Mid(Trim(imagebuf), 1, 1)) * 256 + Asc(Mid(Trim(imagebuf), 2, 1))
                        image = Space(ImageSize)
                        image = Mid(imagebuf, 3, ImageSize)
                        fileNum = FreeFile()


                        FileOpen(fileNum, fileName, OpenMode.Binary, OpenAccess.ReadWrite)
                        FilePut(fileNum, image)
                        FileClose(fileNum)

                        img = Bitmap.FromFile(fileName)

                        PictureBox1.Image = New Bitmap(img)


                        img.Dispose()

                        GC.Collect()

                        HDesc = Mid(dataBuf, 5497, 160).Replace("#", " ").Replace("  ", " ")
                        idCard.Text = Mid(dataBuf, 5657, 14)
                    End If

                    PID2.Text = PID
                    TextBoxPIDex.Text = PID
                    FullName2.Text = FullName
                    FullNameEn.Text = FullNameE
                    Dob2.Text = Dob
                    DobEn2.Text = DobEn
                    Me.Sex.Text = Sex
                    HDesc2.Text = HDesc
                    IssuedDate.Text = IssuedDateT
                    ExpireDate.Text = ExpireDateT
                    IssuedDateEn.Text = IssuedDateE
                    ExpireDateEn.Text = ExpireDateE
                    place.Text = placeT


                    getDateActive()
                Else
                    MessageBox.Show("[" + returnCode.ToString() + "] " + status.ToString())
                End If
            Else
                'TextBoxOutput.Text = TextBoxOutput.Text + Environment.NewLine + "[" + returnCode.ToString() + "] " + status.ToString()
                MessageBox.Show("กรุณาเสียบบัตรประจำตัวประชาชน")
            End If
        Catch ex As Exception
            'Dim btn As Button = CType(sender, Button)
            'MessageBox.Show(ex.Message,
            'btn.Text,
            ' MessageBoxButtons.OK,
            'MessageBoxIcon.Exclamation,
            'MessageBoxDefaultButton.Button1)
        End Try
    End Sub


    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles ToolStripButton3.Click
        Close()
    End Sub


    Private Sub TabControl1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles TabControl1.SelectedIndexChanged
        If TabControl1.SelectedIndex = 0 Then
            ToolStripComboBox1.SelectedIndex = 0
        Else
            ToolStripComboBox1.SelectedIndex = 1
        End If
    End Sub

    Private Sub Button4_Click_1(sender As Object, e As EventArgs) Handles Button4.Click
        clearData()
    End Sub

    Private Sub Button2_Click_1(sender As Object, e As EventArgs) Handles Button2.Click
        clearData()
    End Sub

    Private Sub RadioButton2_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton2.CheckedChanged
        checkRadio()
    End Sub

    Private Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        sendActionPerson("0104")
    End Sub

    Private Sub btnNext_Click(sender As Object, e As EventArgs) Handles btnNext.Click
        sendActionPerson("0103")
    End Sub

    Private Sub ToolStripButton2_Click(sender As Object, e As EventArgs) Handles ToolStripButton2.Click

        Dim path As String = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)

        If ToolStripComboBox1.SelectedIndex = 0 Then
            Dim FILE_NAME As String = path & "\" + PID2.Text + ".txt"
            If System.IO.File.Exists(FILE_NAME) = False Then
                System.IO.File.Create(FILE_NAME).Dispose()
            End If

            Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)

            objWriter.WriteLine("=========== ข้อมูลจากบัตรประชาชน ==============")
            objWriter.WriteLine("เลขประจำตัวประชาชน :" + PID2.Text)
            objWriter.WriteLine("ชื่อ - นามสกุล ไทย :" + FullName2.Text)
            objWriter.WriteLine("ชื่อ - นามสกุล อังกฤษ :" + FullNameEn.Text)
            objWriter.WriteLine("เพศ :" + Sex.Text)
            objWriter.WriteLine("วันเดือนปี เกิด :" + Dob2.Text)
            objWriter.WriteLine("ที่อยู่ :" + HDesc2.Text)
            objWriter.WriteLine("วันที่ออกบัตร :" + IssuedDate.Text)
            objWriter.WriteLine("วันบัตรหมดอายุ :" + ExpireDate.Text)
            objWriter.WriteLine("สถานที่ออกบัตร :" + place.Text)

            objWriter.Close()

            Process.Start(FILE_NAME)

        ElseIf ToolStripComboBox1.SelectedIndex = 1 Then
            Dim FILE_NAME As String = path & "\" + PID.Text + ".txt"
            If System.IO.File.Exists(FILE_NAME) = False Then
                System.IO.File.Create(FILE_NAME).Dispose()
            End If

            Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)

            objWriter.WriteLine("=========== ข้อมูลจากทะเบียนราษฎร์  ==============")
            objWriter.WriteLine("เลขประจำตัวประชาชน :" + PID.Text)
            objWriter.WriteLine("ชื่อ - นามสกุล :" + FullName.Text)
            objWriter.WriteLine("วันเดือนปี เกิด :" + Dob.Text)
            objWriter.WriteLine("เพศ :" + Sex.Text)
            objWriter.WriteLine("สัญชาติ :" + Nat.Text)
            objWriter.WriteLine("ที่อยู่ :" + HDesc.Text)
            objWriter.WriteLine("เลขรหัสประจำบ้าน :" + Hid.Text)
            objWriter.WriteLine("สถานภาพเจ้าบ้าน :" + HStat.Text)
            objWriter.WriteLine("วันที่ย้ายเข้า :" + DMoveIn.Text)
            objWriter.WriteLine("สถานภาพบุคคล :" + PStat.Text)
            objWriter.WriteLine("มารดา ชื่อ :" + MFName.Text)
            objWriter.WriteLine("เลขประจำตัวประชาชน มารดา :" + FPID.Text)
            objWriter.WriteLine("สัญชาติ มารดา :" + MNat.Text)
            objWriter.WriteLine("บิดา ชื่อ :" + FFName.Text)
            objWriter.WriteLine("เลขประจำตัวประชาชน บิดา :" + MPID.Text)
            objWriter.WriteLine("สัญชาติ บิดา :" + FNat.Text)

            objWriter.Close()
            Process.Start(FILE_NAME)

        ElseIf ToolStripComboBox1.SelectedIndex = 2 Then

            Dim FILE_NAME As String = path & "\" + PID2.Text + ".txt"
            If System.IO.File.Exists(FILE_NAME) = False Then
                System.IO.File.Create(FILE_NAME).Dispose()
            End If

            Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)

            objWriter.WriteLine("=========== ข้อมูลจากบัตรประชาชน ==============")
            objWriter.WriteLine("เลขประจำตัวประชาชน :" + PID2.Text)
            objWriter.WriteLine("ชื่อ - นามสกุล ไทย :" + FullName2.Text)
            objWriter.WriteLine("ชื่อ - นามสกุล อังกฤษ :" + FullNameEn.Text)
            objWriter.WriteLine("เพศ :" + Sex.Text)
            objWriter.WriteLine("วันเดือนปี เกิด :" + Dob2.Text)
            objWriter.WriteLine("ที่อยู่ :" + HDesc2.Text)
            objWriter.WriteLine("วันที่ออกบัตร :" + IssuedDate.Text)
            objWriter.WriteLine("วันบัตรหมดอายุ :" + ExpireDate.Text)
            objWriter.WriteLine("สถานที่ออกบัตร :" + place.Text)
            objWriter.WriteLine("               ")
            objWriter.WriteLine("               ")
            objWriter.WriteLine("=========== ข้อมูลจากทะเบียนราษฎร์  ==============")
            objWriter.WriteLine("เลขประจำตัวประชาชน :" + PID.Text)
            objWriter.WriteLine("ชื่อ - นามสกุล :" + FullName.Text)
            objWriter.WriteLine("วันเดือนปี เกิด :" + Dob.Text)
            objWriter.WriteLine("เพศ :" + Sex.Text)
            objWriter.WriteLine("สัญชาติ :" + Nat.Text)
            objWriter.WriteLine("ที่อยู่ :" + HDesc.Text)
            objWriter.WriteLine("เลขรหัสประจำบ้าน :" + Hid.Text)
            objWriter.WriteLine("สถานภาพเจ้าบ้าน :" + HStat.Text)
            objWriter.WriteLine("วันที่ย้ายเข้า :" + DMoveIn.Text)
            objWriter.WriteLine("สถานภาพบุคคล :" + PStat.Text)
            objWriter.WriteLine("มารดา ชื่อ :" + MFName.Text)
            objWriter.WriteLine("เลขประจำตัวประชาชน มารดา :" + FPID.Text)
            objWriter.WriteLine("สัญชาติ มารดา :" + MNat.Text)
            objWriter.WriteLine("บิดา ชื่อ :" + FFName.Text)
            objWriter.WriteLine("เลขประจำตัวประชาชน บิดา :" + MPID.Text)
            objWriter.WriteLine("สัญชาติ บิดา :" + FNat.Text)

            objWriter.Close()
            Process.Start(FILE_NAME)
        End If
    End Sub
End Class