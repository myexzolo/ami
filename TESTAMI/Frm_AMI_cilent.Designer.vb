﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Frm_AMI_cilent
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Frm_AMI_cilent))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.IssuedDate = New System.Windows.Forms.Label()
        Me.IssuedDateEn = New System.Windows.Forms.Label()
        Me.ExpireDate = New System.Windows.Forms.Label()
        Me.ExpireDateEn = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Sex = New System.Windows.Forms.Label()
        Me.Dob2 = New System.Windows.Forms.Label()
        Me.SexEn = New System.Windows.Forms.Label()
        Me.DobEn2 = New System.Windows.Forms.Label()
        Me.FullNameEn = New System.Windows.Forms.Label()
        Me.FullName2 = New System.Windows.Forms.Label()
        Me.PID2 = New System.Windows.Forms.Label()
        Me.dateActive = New System.Windows.Forms.Label()
        Me.place = New System.Windows.Forms.Label()
        Me.HDesc2 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.idCard = New System.Windows.Forms.Label()
        Me.TextBoxPIDex = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.LNAME = New System.Windows.Forms.TextBox()
        Me.FNAME = New System.Windows.Forms.TextBox()
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.btnBack = New System.Windows.Forms.Button()
        Me.btnNext = New System.Windows.Forms.Button()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Sex2 = New System.Windows.Forms.Label()
        Me.FNat = New System.Windows.Forms.Label()
        Me.MNat = New System.Windows.Forms.Label()
        Me.PStat = New System.Windows.Forms.Label()
        Me.Nat = New System.Windows.Forms.Label()
        Me.HStat = New System.Windows.Forms.Label()
        Me.Dob = New System.Windows.Forms.Label()
        Me.FullName = New System.Windows.Forms.Label()
        Me.PID = New System.Windows.Forms.Label()
        Me.Hid = New System.Windows.Forms.Label()
        Me.DMoveIn = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.MPID = New System.Windows.Forms.Label()
        Me.FPID = New System.Windows.Forms.Label()
        Me.FFName = New System.Windows.Forms.Label()
        Me.MFName = New System.Windows.Forms.Label()
        Me.HDesc = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.RStatus = New System.Windows.Forms.Label()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripComboBox1 = New System.Windows.Forms.ToolStripComboBox()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton3 = New System.Windows.Forms.ToolStripButton()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.NotifyIcon1 = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolStripTextBox1 = New System.Windows.Forms.ToolStripTextBox()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripTextBox2 = New System.Windows.Forms.ToolStripTextBox()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("TH SarabunPSK", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.Location = New System.Drawing.Point(20, 70)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(91, 22)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "เครื่องอ่านบัตร :"
        '
        'ComboBox1
        '
        Me.ComboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ComboBox1.Font = New System.Drawing.Font("TH SarabunPSK", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(117, 66)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(352, 30)
        Me.ComboBox1.TabIndex = 2
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Controls.Add(Me.IssuedDate)
        Me.Panel1.Controls.Add(Me.IssuedDateEn)
        Me.Panel1.Controls.Add(Me.ExpireDate)
        Me.Panel1.Controls.Add(Me.ExpireDateEn)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.Label30)
        Me.Panel1.Controls.Add(Me.Label37)
        Me.Panel1.Controls.Add(Me.Label33)
        Me.Panel1.Controls.Add(Me.Label28)
        Me.Panel1.Controls.Add(Me.Label26)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Sex)
        Me.Panel1.Controls.Add(Me.Dob2)
        Me.Panel1.Controls.Add(Me.SexEn)
        Me.Panel1.Controls.Add(Me.DobEn2)
        Me.Panel1.Controls.Add(Me.FullNameEn)
        Me.Panel1.Controls.Add(Me.FullName2)
        Me.Panel1.Controls.Add(Me.PID2)
        Me.Panel1.Controls.Add(Me.dateActive)
        Me.Panel1.Controls.Add(Me.place)
        Me.Panel1.Controls.Add(Me.HDesc2)
        Me.Panel1.Controls.Add(Me.Label14)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.Label27)
        Me.Panel1.Controls.Add(Me.Label17)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.Label13)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.idCard)
        Me.Panel1.Location = New System.Drawing.Point(18, 10)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(644, 419)
        Me.Panel1.TabIndex = 18
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PictureBox1.BackColor = System.Drawing.Color.White
        Me.PictureBox1.BackgroundImage = Global.TESTAMI.My.Resources.Resources.Antu_user
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox1.Location = New System.Drawing.Point(482, 67)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(134, 158)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 18
        Me.PictureBox1.TabStop = False
        '
        'IssuedDate
        '
        Me.IssuedDate.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.IssuedDate.Location = New System.Drawing.Point(195, 261)
        Me.IssuedDate.Name = "IssuedDate"
        Me.IssuedDate.Size = New System.Drawing.Size(152, 25)
        Me.IssuedDate.TabIndex = 48
        Me.IssuedDate.Text = "25/05/2527"
        '
        'IssuedDateEn
        '
        Me.IssuedDateEn.Font = New System.Drawing.Font("TH SarabunPSK", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.IssuedDateEn.ForeColor = System.Drawing.Color.MidnightBlue
        Me.IssuedDateEn.Location = New System.Drawing.Point(195, 286)
        Me.IssuedDateEn.Name = "IssuedDateEn"
        Me.IssuedDateEn.Size = New System.Drawing.Size(155, 27)
        Me.IssuedDateEn.TabIndex = 47
        Me.IssuedDateEn.Text = "25 May 1984"
        '
        'ExpireDate
        '
        Me.ExpireDate.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ExpireDate.Location = New System.Drawing.Point(482, 261)
        Me.ExpireDate.Name = "ExpireDate"
        Me.ExpireDate.Size = New System.Drawing.Size(146, 25)
        Me.ExpireDate.TabIndex = 48
        Me.ExpireDate.Text = "25/05/2527"
        '
        'ExpireDateEn
        '
        Me.ExpireDateEn.Font = New System.Drawing.Font("TH SarabunPSK", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ExpireDateEn.ForeColor = System.Drawing.Color.MidnightBlue
        Me.ExpireDateEn.Location = New System.Drawing.Point(483, 287)
        Me.ExpireDateEn.Name = "ExpireDateEn"
        Me.ExpireDateEn.Size = New System.Drawing.Size(145, 27)
        Me.ExpireDateEn.TabIndex = 47
        Me.ExpireDateEn.Text = "25 May 1984"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label9.Location = New System.Drawing.Point(444, 7)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(32, 24)
        Me.Label9.TabIndex = 40
        Me.Label9.Text = "เพศ"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label30.Location = New System.Drawing.Point(376, 259)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(100, 24)
        Me.Label30.TabIndex = 39
        Me.Label30.Text = "วันบัตรหมดอายุ"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label37.Location = New System.Drawing.Point(88, 363)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(96, 24)
        Me.Label37.TabIndex = 39
        Me.Label37.Text = "วันที่ทำรายการ"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label33.Location = New System.Drawing.Point(85, 325)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(99, 24)
        Me.Label33.TabIndex = 39
        Me.Label33.Text = "สถานที่ออกบัตร"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label28.Location = New System.Drawing.Point(100, 259)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(84, 24)
        Me.Label28.TabIndex = 39
        Me.Label28.Text = "วันที่ออกบัตร"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label26.Location = New System.Drawing.Point(149, 197)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(35, 24)
        Me.Label26.TabIndex = 39
        Me.Label26.Text = "ที่อยู่"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label8.Location = New System.Drawing.Point(94, 135)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(90, 24)
        Me.Label8.TabIndex = 39
        Me.Label8.Text = "วันเดือนปี เกิด"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label5.Location = New System.Drawing.Point(93, 73)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(91, 24)
        Me.Label5.TabIndex = 38
        Me.Label5.Text = "ชื่อ - นามสกุล"
        '
        'Sex
        '
        Me.Sex.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Sex.Location = New System.Drawing.Point(486, 7)
        Me.Sex.Name = "Sex"
        Me.Sex.Size = New System.Drawing.Size(82, 27)
        Me.Sex.TabIndex = 35
        Me.Sex.Text = "ชาย"
        Me.Sex.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Dob2
        '
        Me.Dob2.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Dob2.Location = New System.Drawing.Point(196, 136)
        Me.Dob2.Name = "Dob2"
        Me.Dob2.Size = New System.Drawing.Size(151, 25)
        Me.Dob2.TabIndex = 30
        Me.Dob2.Text = "25/05/2527"
        '
        'SexEn
        '
        Me.SexEn.Font = New System.Drawing.Font("TH SarabunPSK", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.SexEn.ForeColor = System.Drawing.Color.MidnightBlue
        Me.SexEn.Location = New System.Drawing.Point(487, 34)
        Me.SexEn.Name = "SexEn"
        Me.SexEn.Size = New System.Drawing.Size(94, 27)
        Me.SexEn.TabIndex = 29
        Me.SexEn.Text = "Male"
        '
        'DobEn2
        '
        Me.DobEn2.Font = New System.Drawing.Font("TH SarabunPSK", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.DobEn2.ForeColor = System.Drawing.Color.MidnightBlue
        Me.DobEn2.Location = New System.Drawing.Point(196, 161)
        Me.DobEn2.Name = "DobEn2"
        Me.DobEn2.Size = New System.Drawing.Size(151, 27)
        Me.DobEn2.TabIndex = 29
        Me.DobEn2.Text = "25 May 1984"
        '
        'FullNameEn
        '
        Me.FullNameEn.Font = New System.Drawing.Font("TH SarabunPSK", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.FullNameEn.ForeColor = System.Drawing.Color.MidnightBlue
        Me.FullNameEn.Location = New System.Drawing.Point(196, 99)
        Me.FullNameEn.Name = "FullNameEn"
        Me.FullNameEn.Size = New System.Drawing.Size(260, 27)
        Me.FullNameEn.TabIndex = 29
        Me.FullNameEn.Text = "Mr. Suthipan Pongsuwan"
        '
        'FullName2
        '
        Me.FullName2.Font = New System.Drawing.Font("TH SarabunPSK", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.FullName2.ForeColor = System.Drawing.Color.Black
        Me.FullName2.Location = New System.Drawing.Point(194, 69)
        Me.FullName2.Name = "FullName2"
        Me.FullName2.Size = New System.Drawing.Size(260, 35)
        Me.FullName2.TabIndex = 29
        Me.FullName2.Text = "นายสุทธิพันธ์ ป้องสุวรรณ"
        '
        'PID2
        '
        Me.PID2.Font = New System.Drawing.Font("TH SarabunPSK", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.PID2.ForeColor = System.Drawing.Color.Black
        Me.PID2.Location = New System.Drawing.Point(194, 15)
        Me.PID2.Name = "PID2"
        Me.PID2.Size = New System.Drawing.Size(212, 40)
        Me.PID2.TabIndex = 28
        Me.PID2.Text = "1580400006441"
        '
        'dateActive
        '
        Me.dateActive.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.dateActive.Location = New System.Drawing.Point(190, 363)
        Me.dateActive.Name = "dateActive"
        Me.dateActive.Size = New System.Drawing.Size(422, 24)
        Me.dateActive.TabIndex = 19
        Me.dateActive.Text = "20 ธันวาคม 2561  12:00"
        '
        'place
        '
        Me.place.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.place.Location = New System.Drawing.Point(190, 325)
        Me.place.Name = "place"
        Me.place.Size = New System.Drawing.Size(422, 24)
        Me.place.TabIndex = 19
        Me.place.Text = "402 หมู่ 12 ต.บ้านกาศ อ.แม่สะเรียง จ. แม่ฮ่องสอน"
        '
        'HDesc2
        '
        Me.HDesc2.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.HDesc2.Location = New System.Drawing.Point(196, 198)
        Me.HDesc2.Name = "HDesc2"
        Me.HDesc2.Size = New System.Drawing.Size(231, 57)
        Me.HDesc2.TabIndex = 19
        Me.HDesc2.Text = "402 หมู่ 12 ต.บ้านกาศ อ.แม่สะเรียง จ. แม่ฮ่องสอน"
        '
        'Label14
        '
        Me.Label14.BackColor = System.Drawing.Color.Orange
        Me.Label14.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label14.Location = New System.Drawing.Point(356, 286)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(120, 24)
        Me.Label14.TabIndex = 46
        Me.Label14.Text = "Date of Expiry"
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.Color.Orange
        Me.Label12.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label12.Location = New System.Drawing.Point(20, 286)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(164, 24)
        Me.Label12.TabIndex = 46
        Me.Label12.Text = "Date of Issue"
        '
        'Label27
        '
        Me.Label27.BackColor = System.Drawing.Color.Orange
        Me.Label27.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label27.Location = New System.Drawing.Point(20, 224)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(164, 24)
        Me.Label27.TabIndex = 46
        Me.Label27.Text = "Address"
        '
        'Label17
        '
        Me.Label17.BackColor = System.Drawing.Color.Orange
        Me.Label17.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label17.Location = New System.Drawing.Point(20, 162)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(164, 24)
        Me.Label17.TabIndex = 46
        Me.Label17.Text = "Date of Birth"
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.Color.Orange
        Me.Label10.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label10.Location = New System.Drawing.Point(20, 100)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(164, 24)
        Me.Label10.TabIndex = 46
        Me.Label10.Text = "Full Name"
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.Color.Orange
        Me.Label13.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label13.Location = New System.Drawing.Point(412, 34)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(64, 24)
        Me.Label13.TabIndex = 46
        Me.Label13.Text = "Gender"
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.Orange
        Me.Label6.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label6.Location = New System.Drawing.Point(19, 38)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(165, 24)
        Me.Label6.TabIndex = 46
        Me.Label6.Text = "Identification Number"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label4.Location = New System.Drawing.Point(42, 11)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(142, 24)
        Me.Label4.TabIndex = 46
        Me.Label4.Text = "เลขประจำตัวประชาชน"
        '
        'idCard
        '
        Me.idCard.BackColor = System.Drawing.Color.Orange
        Me.idCard.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.idCard.Location = New System.Drawing.Point(481, 224)
        Me.idCard.Name = "idCard"
        Me.idCard.Size = New System.Drawing.Size(134, 25)
        Me.idCard.TabIndex = 46
        Me.idCard.Text = "12234567890"
        Me.idCard.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'TextBoxPIDex
        '
        Me.TextBoxPIDex.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextBoxPIDex.Location = New System.Drawing.Point(184, 70)
        Me.TextBoxPIDex.MaxLength = 13
        Me.TextBoxPIDex.Name = "TextBoxPIDex"
        Me.TextBoxPIDex.Size = New System.Drawing.Size(183, 31)
        Me.TextBoxPIDex.TabIndex = 18
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label7.Location = New System.Drawing.Point(142, 29)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(36, 24)
        Me.Label7.TabIndex = 17
        Me.Label7.Text = "ชื่อ :"
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.RadioButton2)
        Me.GroupBox2.Controls.Add(Me.RadioButton1)
        Me.GroupBox2.Controls.Add(Me.Button4)
        Me.GroupBox2.Controls.Add(Me.Button1)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.LNAME)
        Me.GroupBox2.Controls.Add(Me.FNAME)
        Me.GroupBox2.Controls.Add(Me.TextBoxPIDex)
        Me.GroupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GroupBox2.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(9, 6)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(668, 111)
        Me.GroupBox2.TabIndex = 19
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "สืบค้นข้อมูล"
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Checked = True
        Me.RadioButton2.Location = New System.Drawing.Point(11, 77)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(14, 13)
        Me.RadioButton2.TabIndex = 22
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Location = New System.Drawing.Point(11, 34)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(14, 13)
        Me.RadioButton1.TabIndex = 22
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Button4.Image = Global.TESTAMI.My.Resources.Resources.go_back_arrow
        Me.Button4.Location = New System.Drawing.Point(529, 71)
        Me.Button4.Name = "Button4"
        Me.Button4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Button4.Size = New System.Drawing.Size(120, 32)
        Me.Button4.TabIndex = 21
        Me.Button4.Text = "  ล้างค่า"
        Me.Button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Image = Global.TESTAMI.My.Resources.Resources.uploading_archive
        Me.Button1.Location = New System.Drawing.Point(393, 70)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(120, 32)
        Me.Button1.TabIndex = 19
        Me.Button1.Text = "  ดึงข้อมูล"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label11.Location = New System.Drawing.Point(27, 71)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(151, 24)
        Me.Label11.TabIndex = 17
        Me.Label11.Text = "เลขประจำตัวประชาชน :"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label15.Location = New System.Drawing.Point(376, 29)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(68, 24)
        Me.Label15.TabIndex = 17
        Me.Label15.Text = "นามสกุล :"
        '
        'LNAME
        '
        Me.LNAME.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LNAME.Location = New System.Drawing.Point(447, 26)
        Me.LNAME.MaxLength = 13
        Me.LNAME.Name = "LNAME"
        Me.LNAME.Size = New System.Drawing.Size(202, 31)
        Me.LNAME.TabIndex = 18
        '
        'FNAME
        '
        Me.FNAME.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.FNAME.Location = New System.Drawing.Point(184, 26)
        Me.FNAME.MaxLength = 13
        Me.FNAME.Name = "FNAME"
        Me.FNAME.Size = New System.Drawing.Size(183, 31)
        Me.FNAME.TabIndex = 18
        '
        'PrintDocument1
        '
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Font = New System.Drawing.Font("TH SarabunPSK", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(20, 102)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(692, 523)
        Me.TabControl1.TabIndex = 19
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.White
        Me.TabPage1.Controls.Add(Me.Panel1)
        Me.TabPage1.Controls.Add(Me.Button2)
        Me.TabPage1.Controls.Add(Me.Button5)
        Me.TabPage1.Font = New System.Drawing.Font("TH Sarabun New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TabPage1.Location = New System.Drawing.Point(4, 31)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(684, 488)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = " ข้อมูลจากบัตรประชาชน "
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Button2.Image = Global.TESTAMI.My.Resources.Resources.go_back_arrow
        Me.Button2.Location = New System.Drawing.Point(335, 440)
        Me.Button2.Name = "Button2"
        Me.Button2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Button2.Size = New System.Drawing.Size(128, 33)
        Me.Button2.TabIndex = 20
        Me.Button2.Text = "  ล้างค่า"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Button5.Image = Global.TESTAMI.My.Resources.Resources.uploading_archive
        Me.Button5.Location = New System.Drawing.Point(199, 440)
        Me.Button5.Name = "Button5"
        Me.Button5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Button5.Size = New System.Drawing.Size(128, 33)
        Me.Button5.TabIndex = 20
        Me.Button5.Text = "  อ่านข้อมูล"
        Me.Button5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.Button5.UseVisualStyleBackColor = False
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.White
        Me.TabPage2.Controls.Add(Me.GroupBox3)
        Me.TabPage2.Controls.Add(Me.GroupBox2)
        Me.TabPage2.Location = New System.Drawing.Point(4, 31)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(684, 488)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = " ข้อมูลจากทะเบียนราษฎร์  "
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox3.Controls.Add(Me.Panel3)
        Me.GroupBox3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GroupBox3.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(9, 123)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(668, 359)
        Me.GroupBox3.TabIndex = 4
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "ข้อมูลทะเบียนราษฎร์"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.Transparent
        Me.Panel3.Controls.Add(Me.btnBack)
        Me.Panel3.Controls.Add(Me.btnNext)
        Me.Panel3.Controls.Add(Me.Label25)
        Me.Panel3.Controls.Add(Me.Label2)
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Controls.Add(Me.Label16)
        Me.Panel3.Controls.Add(Me.Label18)
        Me.Panel3.Controls.Add(Me.Label19)
        Me.Panel3.Controls.Add(Me.Label21)
        Me.Panel3.Controls.Add(Me.Label22)
        Me.Panel3.Controls.Add(Me.Label23)
        Me.Panel3.Controls.Add(Me.Label24)
        Me.Panel3.Controls.Add(Me.Sex2)
        Me.Panel3.Controls.Add(Me.FNat)
        Me.Panel3.Controls.Add(Me.MNat)
        Me.Panel3.Controls.Add(Me.PStat)
        Me.Panel3.Controls.Add(Me.Nat)
        Me.Panel3.Controls.Add(Me.HStat)
        Me.Panel3.Controls.Add(Me.Dob)
        Me.Panel3.Controls.Add(Me.FullName)
        Me.Panel3.Controls.Add(Me.PID)
        Me.Panel3.Controls.Add(Me.Hid)
        Me.Panel3.Controls.Add(Me.DMoveIn)
        Me.Panel3.Controls.Add(Me.Label35)
        Me.Panel3.Controls.Add(Me.Label36)
        Me.Panel3.Controls.Add(Me.MPID)
        Me.Panel3.Controls.Add(Me.FPID)
        Me.Panel3.Controls.Add(Me.FFName)
        Me.Panel3.Controls.Add(Me.MFName)
        Me.Panel3.Controls.Add(Me.HDesc)
        Me.Panel3.Controls.Add(Me.Label42)
        Me.Panel3.Controls.Add(Me.Label43)
        Me.Panel3.Location = New System.Drawing.Point(14, 28)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(644, 321)
        Me.Panel3.TabIndex = 18
        '
        'btnBack
        '
        Me.btnBack.Image = Global.TESTAMI.My.Resources.Resources.back
        Me.btnBack.Location = New System.Drawing.Point(557, 5)
        Me.btnBack.Name = "btnBack"
        Me.btnBack.Size = New System.Drawing.Size(36, 36)
        Me.btnBack.TabIndex = 47
        Me.btnBack.UseVisualStyleBackColor = True
        Me.btnBack.Visible = False
        '
        'btnNext
        '
        Me.btnNext.Image = Global.TESTAMI.My.Resources.Resources.right_arrow
        Me.btnNext.Location = New System.Drawing.Point(599, 5)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(36, 36)
        Me.btnNext.TabIndex = 47
        Me.btnNext.UseVisualStyleBackColor = True
        Me.btnNext.Visible = False
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label25.Location = New System.Drawing.Point(411, 205)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(110, 24)
        Me.Label25.TabIndex = 44
        Me.Label25.Text = "สถานภาพบุคคล :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(400, 165)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(121, 24)
        Me.Label2.TabIndex = 44
        Me.Label2.Text = "สถานภาพเจ้าบ้าน :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label3.Location = New System.Drawing.Point(459, 285)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(62, 24)
        Me.Label3.TabIndex = 43
        Me.Label3.Text = "สัญชาติ :"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label16.Location = New System.Drawing.Point(459, 245)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(62, 24)
        Me.Label16.TabIndex = 42
        Me.Label16.Text = "สัญชาติ :"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label18.Location = New System.Drawing.Point(459, 85)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(62, 24)
        Me.Label18.TabIndex = 41
        Me.Label18.Text = "สัญชาติ :"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label19.Location = New System.Drawing.Point(480, 45)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(41, 24)
        Me.Label19.TabIndex = 40
        Me.Label19.Text = "เพศ :"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label21.Location = New System.Drawing.Point(70, 85)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(99, 24)
        Me.Label21.TabIndex = 39
        Me.Label21.Text = "วันเดือนปี เกิด :"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label22.Location = New System.Drawing.Point(94, 45)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(75, 24)
        Me.Label22.TabIndex = 38
        Me.Label22.Text = "ชื่อ - สกุล :"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label23.Location = New System.Drawing.Point(42, 165)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(127, 24)
        Me.Label23.TabIndex = 37
        Me.Label23.Text = "เลขรหัสประจำบ้าน :"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label24.Location = New System.Drawing.Point(104, 285)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(65, 24)
        Me.Label24.TabIndex = 36
        Me.Label24.Text = "บิดา ชื่อ :"
        '
        'Sex2
        '
        Me.Sex2.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Sex2.Location = New System.Drawing.Point(527, 44)
        Me.Sex2.Name = "Sex2"
        Me.Sex2.Size = New System.Drawing.Size(89, 25)
        Me.Sex2.TabIndex = 35
        Me.Sex2.Text = "ชาย"
        '
        'FNat
        '
        Me.FNat.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.FNat.Location = New System.Drawing.Point(527, 284)
        Me.FNat.Name = "FNat"
        Me.FNat.Size = New System.Drawing.Size(86, 25)
        Me.FNat.TabIndex = 34
        Me.FNat.Text = "ไทย"
        '
        'MNat
        '
        Me.MNat.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.MNat.Location = New System.Drawing.Point(527, 244)
        Me.MNat.Name = "MNat"
        Me.MNat.Size = New System.Drawing.Size(86, 25)
        Me.MNat.TabIndex = 33
        Me.MNat.Text = "ไทย"
        '
        'PStat
        '
        Me.PStat.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.PStat.Location = New System.Drawing.Point(527, 206)
        Me.PStat.Name = "PStat"
        Me.PStat.Size = New System.Drawing.Size(86, 25)
        Me.PStat.TabIndex = 32
        Me.PStat.Text = "ผู้อาศัย"
        '
        'Nat
        '
        Me.Nat.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Nat.Location = New System.Drawing.Point(527, 84)
        Me.Nat.Name = "Nat"
        Me.Nat.Size = New System.Drawing.Size(86, 25)
        Me.Nat.TabIndex = 45
        Me.Nat.Text = "ไทย"
        '
        'HStat
        '
        Me.HStat.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.HStat.Location = New System.Drawing.Point(527, 165)
        Me.HStat.Name = "HStat"
        Me.HStat.Size = New System.Drawing.Size(86, 25)
        Me.HStat.TabIndex = 32
        Me.HStat.Text = "ผู้อาศัย"
        '
        'Dob
        '
        Me.Dob.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Dob.Location = New System.Drawing.Point(172, 85)
        Me.Dob.Name = "Dob"
        Me.Dob.Size = New System.Drawing.Size(206, 25)
        Me.Dob.TabIndex = 30
        Me.Dob.Text = "25/05/2527"
        '
        'FullName
        '
        Me.FullName.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.FullName.Location = New System.Drawing.Point(172, 45)
        Me.FullName.Name = "FullName"
        Me.FullName.Size = New System.Drawing.Size(260, 25)
        Me.FullName.TabIndex = 29
        Me.FullName.Text = "นายสุทธิพันธ์ ป้องสุวรรณ"
        '
        'PID
        '
        Me.PID.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.PID.Location = New System.Drawing.Point(172, 5)
        Me.PID.Name = "PID"
        Me.PID.Size = New System.Drawing.Size(164, 25)
        Me.PID.TabIndex = 28
        Me.PID.Text = "1580400006441"
        '
        'Hid
        '
        Me.Hid.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Hid.Location = New System.Drawing.Point(172, 165)
        Me.Hid.Name = "Hid"
        Me.Hid.Size = New System.Drawing.Size(234, 25)
        Me.Hid.TabIndex = 27
        Me.Hid.Text = "123456"
        '
        'DMoveIn
        '
        Me.DMoveIn.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.DMoveIn.Location = New System.Drawing.Point(172, 205)
        Me.DMoveIn.Name = "DMoveIn"
        Me.DMoveIn.Size = New System.Drawing.Size(167, 25)
        Me.DMoveIn.TabIndex = 26
        Me.DMoveIn.Text = "28/11/2561"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label35.Location = New System.Drawing.Point(81, 205)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(88, 24)
        Me.Label35.TabIndex = 25
        Me.Label35.Text = "วันที่ย้ายเข้า :"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label36.Location = New System.Drawing.Point(125, 125)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(44, 24)
        Me.Label36.TabIndex = 24
        Me.Label36.Text = "ที่อยู่ :"
        '
        'MPID
        '
        Me.MPID.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.MPID.Location = New System.Drawing.Point(315, 284)
        Me.MPID.Name = "MPID"
        Me.MPID.Size = New System.Drawing.Size(133, 25)
        Me.MPID.TabIndex = 23
        Me.MPID.Text = "3580400152304"
        '
        'FPID
        '
        Me.FPID.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.FPID.Location = New System.Drawing.Point(315, 245)
        Me.FPID.Name = "FPID"
        Me.FPID.Size = New System.Drawing.Size(133, 25)
        Me.FPID.TabIndex = 22
        Me.FPID.Text = "3580400152304"
        '
        'FFName
        '
        Me.FFName.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.FFName.Location = New System.Drawing.Point(172, 284)
        Me.FFName.Name = "FFName"
        Me.FFName.Size = New System.Drawing.Size(137, 25)
        Me.FFName.TabIndex = 21
        Me.FFName.Text = "อุดม"
        '
        'MFName
        '
        Me.MFName.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.MFName.Location = New System.Drawing.Point(172, 245)
        Me.MFName.Name = "MFName"
        Me.MFName.Size = New System.Drawing.Size(137, 25)
        Me.MFName.TabIndex = 20
        Me.MFName.Text = "สุนทรี "
        '
        'HDesc
        '
        Me.HDesc.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.HDesc.Location = New System.Drawing.Point(172, 125)
        Me.HDesc.Name = "HDesc"
        Me.HDesc.Size = New System.Drawing.Size(457, 25)
        Me.HDesc.TabIndex = 19
        Me.HDesc.Text = "402 หมู่ 12 ต.บ้านกาศ อ.แม่สะเรียง จ.แม่ฮ่องสอน"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label42.Location = New System.Drawing.Point(90, 245)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(79, 24)
        Me.Label42.TabIndex = 31
        Me.Label42.Text = "มารดา ชื่อ :"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label43.Location = New System.Drawing.Point(18, 5)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(151, 24)
        Me.Label43.TabIndex = 46
        Me.Label43.Text = "เลขประจำตัวประชาชน :"
        '
        'RStatus
        '
        Me.RStatus.Font = New System.Drawing.Font("TH SarabunPSK", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.RStatus.Location = New System.Drawing.Point(0, 626)
        Me.RStatus.Name = "RStatus"
        Me.RStatus.Size = New System.Drawing.Size(734, 25)
        Me.RStatus.TabIndex = 29
        Me.RStatus.Text = "Mess"
        Me.RStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ToolStrip1
        '
        Me.ToolStrip1.AutoSize = False
        Me.ToolStrip1.BackColor = System.Drawing.Color.Orange
        Me.ToolStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(32, 32)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripComboBox1, Me.ToolStripSeparator3, Me.ToolStripButton1, Me.ToolStripButton2, Me.ToolStripSeparator1, Me.ToolStripButton3})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Padding = New System.Windows.Forms.Padding(0, 0, 2, 0)
        Me.ToolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.ToolStrip1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ToolStrip1.Size = New System.Drawing.Size(734, 54)
        Me.ToolStrip1.TabIndex = 22
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripComboBox1
        '
        Me.ToolStripComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ToolStripComboBox1.Font = New System.Drawing.Font("TH SarabunPSK", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripComboBox1.Items.AddRange(New Object() {"ข้อมูลจากบัตรประชาชน", "ข้อมูลจากทะเบียนราษฎร์", "ข้อมูลทั้งหมด"})
        Me.ToolStripComboBox1.Name = "ToolStripComboBox1"
        Me.ToolStripComboBox1.Padding = New System.Windows.Forms.Padding(10)
        Me.ToolStripComboBox1.Size = New System.Drawing.Size(110, 54)
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 54)
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton1.Font = New System.Drawing.Font("TH SarabunPSK", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.Image = Global.TESTAMI.My.Resources.Resources.printer
        Me.ToolStripButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Padding = New System.Windows.Forms.Padding(10)
        Me.ToolStripButton1.Size = New System.Drawing.Size(56, 51)
        Me.ToolStripButton1.Text = "พิมพ์"
        Me.ToolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.ToolStripButton1.ToolTipText = "พิมพ์"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton2.Image = Global.TESTAMI.My.Resources.Resources.report__1_
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Padding = New System.Windows.Forms.Padding(10)
        Me.ToolStripButton2.Size = New System.Drawing.Size(56, 51)
        Me.ToolStripButton2.Text = "บันทึก"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 54)
        '
        'ToolStripButton3
        '
        Me.ToolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton3.Font = New System.Drawing.Font("TH SarabunPSK", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton3.Image = Global.TESTAMI.My.Resources.Resources._exit
        Me.ToolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton3.Name = "ToolStripButton3"
        Me.ToolStripButton3.Padding = New System.Windows.Forms.Padding(10)
        Me.ToolStripButton3.Size = New System.Drawing.Size(56, 51)
        Me.ToolStripButton3.Text = "ปิดโปรแกรม"
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("TH SarabunPSK", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Button3.Image = Global.TESTAMI.My.Resources.Resources.search1
        Me.Button3.Location = New System.Drawing.Point(475, 65)
        Me.Button3.Name = "Button3"
        Me.Button3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Button3.Size = New System.Drawing.Size(87, 31)
        Me.Button3.TabIndex = 20
        Me.Button3.Text = "ค้นหา"
        Me.Button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.Button3.UseVisualStyleBackColor = False
        '
        'NotifyIcon1
        '
        Me.NotifyIcon1.ContextMenuStrip = Me.ContextMenuStrip1
        Me.NotifyIcon1.Icon = CType(resources.GetObject("NotifyIcon1.Icon"), System.Drawing.Icon)
        Me.NotifyIcon1.Text = "ระบบสารสนเทศข้อมูลบุคล"
        Me.NotifyIcon1.Visible = True
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripTextBox1, Me.ToolStripSeparator2, Me.ToolStripTextBox2})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(161, 60)
        '
        'ToolStripTextBox1
        '
        Me.ToolStripTextBox1.Name = "ToolStripTextBox1"
        Me.ToolStripTextBox1.Size = New System.Drawing.Size(100, 23)
        Me.ToolStripTextBox1.Text = "Open"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(157, 6)
        '
        'ToolStripTextBox2
        '
        Me.ToolStripTextBox2.Name = "ToolStripTextBox2"
        Me.ToolStripTextBox2.Size = New System.Drawing.Size(100, 23)
        Me.ToolStripTextBox2.Text = "Close"
        '
        'BackgroundWorker1
        '
        '
        'ComboBox2
        '
        Me.ComboBox2.ForeColor = System.Drawing.Color.Green
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(568, 71)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(124, 21)
        Me.ComboBox2.TabIndex = 38
        Me.ComboBox2.Visible = False
        '
        'Frm_AMI_cilent
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(734, 651)
        Me.Controls.Add(Me.ComboBox2)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.RStatus)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(750, 700)
        Me.Name = "Frm_AMI_cilent"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "ระบบสารสนเทศข้อมูลบุคคล  V1.02"
        Me.WindowState = System.Windows.Forms.FormWindowState.Minimized
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.ContextMenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As Label
    Friend WithEvents ComboBox1 As ComboBox
    Friend WithEvents TextBoxPIDex As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Button1 As Button
    Friend WithEvents Button3 As Button
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Sex As Label
    Friend WithEvents FullName2 As Label
    Friend WithEvents PID2 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents PrintDocument1 As Printing.PrintDocument
    Private WithEvents Dob2 As Label
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents Sex2 As Label
    Friend WithEvents FNat As Label
    Friend WithEvents MNat As Label
    Friend WithEvents Nat As Label
    Friend WithEvents HStat As Label
    Private WithEvents Dob As Label
    Friend WithEvents FullName As Label
    Friend WithEvents PID As Label
    Friend WithEvents Hid As Label
    Friend WithEvents DMoveIn As Label
    Friend WithEvents Label35 As Label
    Friend WithEvents Label36 As Label
    Friend WithEvents MPID As Label
    Friend WithEvents FPID As Label
    Friend WithEvents FFName As Label
    Friend WithEvents MFName As Label
    Friend WithEvents HDesc As Label
    Friend WithEvents Label42 As Label
    Friend WithEvents Label43 As Label
    Friend WithEvents HDesc2 As Label
    Friend WithEvents Button5 As Button
    Public WithEvents PictureBox1 As PictureBox
    Friend WithEvents ToolStrip1 As ToolStrip
    Friend WithEvents ToolStripButton1 As ToolStripButton
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents Label10 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents FullNameEn As Label
    Friend WithEvents ToolStripComboBox1 As ToolStripComboBox
    Friend WithEvents SexEn As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents ToolStripButton2 As ToolStripButton
    Friend WithEvents ToolStripButton3 As ToolStripButton
    Friend WithEvents Label26 As Label
    Friend WithEvents DobEn2 As Label
    Friend WithEvents idCard As Label
    Friend WithEvents Label30 As Label
    Friend WithEvents Label28 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label27 As Label
    Private WithEvents IssuedDate As Label
    Friend WithEvents IssuedDateEn As Label
    Private WithEvents ExpireDate As Label
    Friend WithEvents ExpireDateEn As Label
    Friend WithEvents Label37 As Label
    Friend WithEvents Label33 As Label
    Friend WithEvents dateActive As Label
    Friend WithEvents place As Label
    Friend WithEvents Button2 As Button
    Friend WithEvents Button4 As Button
    Friend WithEvents Label11 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents LNAME As TextBox
    Friend WithEvents FNAME As TextBox
    Friend WithEvents RStatus As Label
    Friend WithEvents RadioButton2 As RadioButton
    Friend WithEvents RadioButton1 As RadioButton
    Friend WithEvents Label25 As Label
    Friend WithEvents PStat As Label
    Friend WithEvents btnBack As Button
    Friend WithEvents btnNext As Button
    Friend WithEvents NotifyIcon1 As NotifyIcon
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents ToolStripTextBox1 As ToolStripTextBox
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents ToolStripTextBox2 As ToolStripTextBox
    Friend WithEvents ToolStripSeparator3 As ToolStripSeparator
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents ComboBox2 As ComboBox
End Class
