﻿Imports System.Net
Imports System.Collections.Specialized
Imports System.Globalization

Public Class agent
    Dim list_Reader As String
    Dim status As Integer = -99
    Dim returnCode As Integer = 99
    'Dim ReturnValue As Short = 99
    Dim scapi_stt As New SCAPI_STATUS
    Dim ami_stt As New AMI_STATUS
    Dim PID As String
    Dim CID As String
    'Dim LKOfficeCode As String
    Dim Output As String
    Dim KEY As String
    Dim TextBox4 As String
    Dim TextBox5 As String
    Dim TKey As String = ""


    Dim PATH As String = ""
    Dim namePic As String = ""

    Dim ReaderCardList(2) As String

    Dim img As Bitmap

    Dim Panel2 As Panel

    Dim LKOfficeCode As String = System.Configuration.ConfigurationManager.AppSettings.Get("LKOfficeCode").ToString()
    Dim AMI_HOST As String = System.Configuration.ConfigurationManager.AppSettings.Get("AMI_HOST").ToString()
    Dim AMI_SERVICE As String = System.Configuration.ConfigurationManager.AppSettings.Get("AMI_SERVICE").ToString()
    Dim T_KEY As String = System.Configuration.ConfigurationManager.AppSettings.Get("T_KEY").ToString()
    Dim READER_OFFICER As String = System.Configuration.ConfigurationManager.AppSettings.Get("READER_OFFICER").ToString()
    Dim READER_NATIONAL As String = System.Configuration.ConfigurationManager.AppSettings.Get("READER_NATIONAL").ToString()
    Dim AUTH_NATIONAL As String = System.Configuration.ConfigurationManager.AppSettings.Get("AUTH_NATIONAL").ToString()
    Dim READER_SMARTCARD As String = System.Configuration.ConfigurationManager.AppSettings.Get("READER_SMARTCARD").ToString()
    Dim TEST As String = System.Configuration.ConfigurationManager.AppSettings.Get("TEST").ToString()

    Dim rc9080 As SCAPI.Recive9080
    Dim rc9081 As SCAPI.Recive9081

    Private Sub agent_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim Amihost = Environment.GetEnvironmentVariable("AMI_HOST")
        Dim AmiService As String = Environment.GetEnvironmentVariable("AMI_SERVICE")

        If Amihost = Nothing Then
            System.Environment.SetEnvironmentVariable("AMI_HOST", AMI_HOST)
        End If

        If AmiService = Nothing Then
            System.Environment.SetEnvironmentVariable("AMI_SERVICE", AMI_SERVICE)
        End If

        PATH = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)


        checKProcessIM()
        BackgroundWorker1.RunWorkerAsync()
    End Sub

    Private Sub checKProcessIM()
        Try
            Dim app_exe As String = "lm.exe"
            Dim Process As Object
            For Each Process In GetObject("winmgmts:").ExecQuery("Select Name from Win32_Process Where Name = '" & app_exe & "'")
                Process.Terminate
            Next
        Catch ex As Exception

        End Try
    End Sub


    Private Sub BackgroundWorker1_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = False
        Dim prefixes(0) As String

        prefixes(0) = "http://localhost:2563/Readcard/"

        Me.Location = New Point(Screen.PrimaryScreen.WorkingArea.Width - 330, Screen.PrimaryScreen.WorkingArea.Height - 246)
        If READER_SMARTCARD = "Y" Then
            ProcessRequestsSmartcard(prefixes)
        Else
            ProcessRequests(prefixes)
        End If

    End Sub

    Private Sub ReadSmartCard()
        returnCode = 99
        status = -99
        Try
            list_Reader = Space(1000) ' จองหน่วยความจำสำหรับเก็บชื่อ Reader ที่ได้
            returnCode = SCAPI.ListReader(list_Reader, status)
            If returnCode = 0 Then
                list_Reader = list_Reader.Trim()
            Else
                checKProcessIM()
                DataAMI.Status = "N"
                DataAMI.Message = "ตรวจสอบเครื่องอ่านบัตร"
                Exit Sub
            End If


            Dim num As Integer = 0

            While list_Reader.Length > 0
                If (list_Reader <> vbNullChar) Then
                    Dim nn As String = list_Reader.Substring(0, 2)
                    Dim ll As Integer = Integer.Parse(nn)

                    ReaderCardList(num) = list_Reader.Substring(2, ll)
                    list_Reader = list_Reader.Substring(ll + 2)
                    num += 1
                Else
                    list_Reader = ""
                End If
            End While

            returnCode = SCAPI.OpenReader(ReaderCardList(0), status)

            If returnCode = 0 Then
                returnCode = 99
                status = -99

                Dim atr As String = Space(100)
                Dim atr_len As Integer = 0
                Dim timeOut As Integer = 100
                Dim card_type As Integer = -999

                returnCode = SCAPI.GetCardStatus(atr, atr_len, timeOut, card_type, status)



                'TextBoxOutput.Text = TextBoxOutput.Text + Environment.NewLine + "Get Card Status [" + returnCode.ToString() + "] " + status.ToString()
                'TextBoxOutput.Text = TextBoxOutput.Text + Environment.NewLine + "[ART] " + atr.Trim()

                returnCode = 99
                status = -99

                Dim aid_bin(64) As Byte
                Dim aid_bin_len As Integer
                Dim util As New Utilities

                util.Str2Bin(SCAPI.MOI_AID, aid_bin, aid_bin_len)
                returnCode = SCAPI.SelectApplet(aid_bin(0), aid_bin_len, status)

                If returnCode = 0 Then
                    returnCode = 99
                    status = -99

                    Dim PID As String = ""
                    Dim NAME_TH As String = ""
                    Dim NAME_EN As String = ""
                    Dim DOB As String = ""
                    Dim GENDER As String = ""
                    Dim CardNumber As String = ""
                    Dim PlaceIssue As String = ""
                    Dim IssueCode As String = ""
                    Dim DateIssue As String = ""
                    Dim DateExpiry As String = ""
                    Dim TypeCard As String = ""
                    Dim Address As String = ""
                    Dim HID As String = ""
                    Dim Image As String = ""
                    Dim numberImage As String = ""
                    Dim SignRegistrar As String = ""

                    Dim block_id, offset As Integer
                    Dim dataBuf As String
                    Dim data_size As Integer

                    block_id = 0
                    offset = 0
                    data_size = 5927
                    dataBuf = Space(5930)

                    returnCode = SCAPI.ReadData(block_id, offset, data_size, dataBuf, status)

                    Dim Version As String = dataBuf.Substring(0, 4)
                    If Version = "0003" Then
                        PID = dataBuf.Substring(4, 13)
                        NAME_TH = dataBuf.Substring(17, 100)
                        NAME_EN = dataBuf.Substring(117, 100)
                        DOB = dataBuf.Substring(217, 8)
                        GENDER = dataBuf.Substring(225, 1) ' 1 = ชาย, 2 = หญิง
                        CardNumber = dataBuf.Substring(226, 20)
                        PlaceIssue = dataBuf.Substring(246, 100)
                        IssueCode = dataBuf.Substring(346, 13)
                        DateIssue = dataBuf.Substring(359, 8)
                        DateExpiry = dataBuf.Substring(367, 8)
                        TypeCard = dataBuf.Substring(375, 2)
                        Image = dataBuf.Substring(377, 5120)
                        Address = dataBuf.Substring(5497, 160)
                        numberImage = dataBuf.Substring(5657, 14)
                        SignRegistrar = dataBuf.Substring(5671, 256)

                    ElseIf Version = "0002" Then
                        Address = dataBuf.Substring(4, 150)
                        block_id = 1
                        offset = 0
                        data_size = 5927
                        dataBuf = Space(5930)

                        returnCode = SCAPI.ReadData(block_id, offset, data_size, dataBuf, status)

                        PID = dataBuf.Substring(4, 13)
                        NAME_TH = dataBuf.Substring(17, 100)
                        NAME_EN = dataBuf.Substring(117, 100)
                        DOB = dataBuf.Substring(217, 8)
                        GENDER = dataBuf.Substring(225, 1) ' 1 = ชาย, 2 = หญิง
                        CardNumber = dataBuf.Substring(226, 20)
                        PlaceIssue = dataBuf.Substring(246, 100)
                        IssueCode = dataBuf.Substring(346, 13)
                        DateIssue = dataBuf.Substring(359, 8)
                        DateExpiry = dataBuf.Substring(367, 8)
                        TypeCard = dataBuf.Substring(375, 2)
                        Image = dataBuf.Substring(377, 5120)

                    ElseIf Version = "0001" Then
                        HID = dataBuf.Substring(4, 11)
                        Address = dataBuf.Substring(15, 160)
                        block_id = 1
                        offset = 0
                        data_size = 5927
                        dataBuf = Space(5930)

                        returnCode = SCAPI.ReadData(block_id, offset, data_size, dataBuf, status)

                        PID = dataBuf.Substring(4, 13)
                        NAME_TH = dataBuf.Substring(17, 100)
                        NAME_EN = dataBuf.Substring(117, 100)
                        DOB = dataBuf.Substring(217, 8)
                        GENDER = dataBuf.Substring(225, 1) ' 1 = ชาย, 2 = หญิง
                        CardNumber = dataBuf.Substring(226, 20)
                        PlaceIssue = dataBuf.Substring(246, 100)
                        IssueCode = dataBuf.Substring(346, 13)
                        DateIssue = dataBuf.Substring(359, 8)
                        DateExpiry = dataBuf.Substring(367, 8)
                        TypeCard = dataBuf.Substring(375, 2)
                        Image = dataBuf.Substring(377, 5120)
                        numberImage = dataBuf.Substring(5657, 14)
                        SignRegistrar = dataBuf.Substring(5671, 256)
                    End If
                    Dim NAME_TH_ARR() As String
                    NAME_TH_ARR = NAME_TH.Split("#")
                    Dim titleName = NAME_TH_ARR(0)
                    Dim firstName = NAME_TH_ARR(1)
                    Dim middleName = NAME_TH_ARR(2)
                    Dim lastName = NAME_TH_ARR(3)

                    Dim NAME_EN_ARR() As String
                    NAME_EN_ARR = NAME_EN.Split("#")
                    Dim titleName_en = NAME_EN_ARR(0)
                    Dim firstName_en = NAME_EN_ARR(1)
                    Dim middleName_en = NAME_EN_ARR(2)
                    Dim lastName_en = NAME_EN_ARR(3)

                    Dim Address_ARR() As String
                    Address_ARR = Address.Split("#")
                    Dim houseNo = Address_ARR(0)
                    Dim villageNo = Address_ARR(1)
                    Dim alleyWay = Address_ARR(2)
                    Dim alley = Address_ARR(3)
                    Dim road = Address_ARR(4)
                    Dim subdistrict = Address_ARR(5)
                    Dim district = Address_ARR(6)
                    Dim province = Address_ARR(7)

                    Dim byte_img() As Byte = System.Text.Encoding.Default.GetBytes(Image, 2, Image.Length - 2)
                    Dim PicBase64 As String = Convert.ToBase64String(byte_img, 0, byte_img.Length)

                    Dim byte_sign() As Byte = System.Text.Encoding.Default.GetBytes(SignRegistrar, 2, SignRegistrar.Length - 2)
                    Dim SignRegistrarBase64 As String = Convert.ToBase64String(byte_sign, 0, byte_sign.Length)

                    Dim responseJson As String
                    responseJson = "{""version"":""" & Version.Trim() & """,""personalId"":""" & PID.Trim() &
                        """,""titleName"":""" & titleName.Trim() & """,""firstName"":""" & firstName.Trim() & """,""middleName"":""" & middleName.Trim() & """,""lastName"":""" & lastName.Trim() &
                        """,""titleName_en"":""" & titleName_en.Trim() & """,""firstName_en"":""" & firstName_en.Trim() & """,""middleName_en"":""" & middleName_en.Trim() & """,""lastName_en"":""" & lastName_en.Trim() &
                        """,""dateOfBirth"":""" & DOB.Trim() & """,""genderCode"":""" & GENDER.Trim() & """,""cardNumber"":""" & CardNumber.Trim() &
                        """,""placeIssue"":""" & PlaceIssue.Trim() & """,""issueCode"":""" & IssueCode.Trim() & """,""dateIssue"":""" & DateIssue.Trim() &
                        """,""dateExpiry"":""" & DateExpiry.Trim() & """,""typeCard"":""" & TypeCard.Trim() & """,""image"":""" & PicBase64.Trim() &
                        """,""numberImage"":""" & numberImage.Trim() & """,""signRegistrar"":""" & SignRegistrarBase64.Trim() & """,""HID"":""" & HID.Trim() &
                        """,""houseNo"":""" & houseNo.Trim() & """,""villageNo"":""" & villageNo.Trim() & """,""alleyWay"":""" & alleyWay.Trim() &
                        """,""alley"":""" & alley.Trim() & """,""road"":""" & road.Trim() & """,""subdistrict"":""" & subdistrict.Trim() &
                        """,""district"":""" & district.Trim() & """,""province"":""" & province.Trim() & """}"
                    DataAMI.Json = responseJson
                    DataAMI.Message = "Successful"
                    DataAMI.Status = "Y"


                    'TextBoxPID.Text = PID

                    util.Str2Bin(SCAPI.ADM_AID, aid_bin, aid_bin_len)
                    'returnCode = SCAPI.SelectApplet(aid_bin(0), aid_bin_len, status)

                    'Dim c_id, pre_perso, perso, chip, os As String

                    'c_id = Space(16)
                    'pre_perso = Space(20)
                    'perso = Space(20)
                    'chip = Space(20)
                    'os = Space(20)

                    'returnCode = SCAPI.GetCardInfo(c_id, chip, os, pre_perso, perso, status)
                    'TextBoxCID.Text = cid
                    'CID = c_id

                Else
                    DataAMI.Message = "Failure"
                    DataAMI.Status = "N"
                End If
            Else

                DataAMI.Message = "กรุณาเสียบบัตรประจำตัวประชาชน"
                DataAMI.Status = "N"
            End If
        Catch ex As Exception
            DataAMI.Status = "N"
        End Try
    End Sub


    Private Sub clearData()
        DataAMI.PID = ""
        DataAMI.OFFICE_CODE = ""
        DataAMI.VERSION_CODE = ""
        DataAMI.SERVICE_CODE = ""
        DataAMI.Status = "N"
        DataAMI.Message = ""
        DataAMI.Json = ""
    End Sub


    Private Sub ProcessRequestsSmartcard(ByVal prefixes() As String)

        If Not System.Net.HttpListener.IsSupported Then

            Console.WriteLine("Windows XP SP2, Server 2003, or higher is required to  & use the HttpListener class.")

            Exit Sub

        End If

        ' URI prefixes are required,

        If prefixes Is Nothing OrElse prefixes.Length = 0 Then

            Throw New ArgumentException("prefixes")

        End If

        ' Create a listener and add the prefixes.

        Dim listener As System.Net.HttpListener = New System.Net.HttpListener()

        For Each s As String In prefixes

            listener.Prefixes.Add(s)

        Next

        Try
            ' Start the listener to begin listening for requests.

            listener.Start()

            Console.WriteLine("Listening ReadSmartCard ...")

            Dim numRequestsToBeHandled As Integer = 10



            For i As Integer = 0 To numRequestsToBeHandled

                Dim response As HttpListenerResponse = Nothing

                Try

                    ' Note: GetContext blocks while waiting for a request.

                    Dim context As HttpListenerContext = listener.GetContext()

                    ' Create the response.

                    response = context.Response

                    If TEST = "Y" Then
                        DataAMI.Json = "{""version"":""0003"",""personalId"":""1580400006543" &
                        """,""titleName"":""นาย"",""firstName"":""สุทธิพันธ์"",""middleName"":"""",""lastName"":""ป้องสุวรรณ" &
                        """,""titleName_en"":""Mr."",""firstName_en"":""Suthipan"",""middleName_en"":"""",""lastName_en"":""Pongsuwan" &
                        """,""dateOfBirth"":""25270525"",""genderCode"":""1"",""cardNumber"":""" &
                        """,""placeIssue"":""ท้องถิ่นเทศบาลนครนนทบุรี/นนทบุรี"",""issueCode"":"""",""dateIssue"":""25600818" &
                        """,""dateExpiry"":""25690524"",""typeCard"":""01"",""image"":""" &
                        """,""numberImage"":"""",""signRegistrar"":"""",""HID"":""" &
                        """,""houseNo"":""402"",""villageNo"":""หมู่ที่ 12"",""alleyWay"":""" &
                        """,""alley"":"""",""road"":"""",""subdistrict"":""ตำบลบ้านกาศ" &
                        """,""district"":""อำเภอแม่สะเรียง"",""province"":""จังหวัดแม่ฮ่องสอน""}"

                        DataAMI.Message = "Successful"
                        DataAMI.Status = "Y"
                    Else
                        clearData()
                        DataAMI.Status = "N"
                        ReadSmartCard()
                    End If


                    Dim responseString As String = "<HTML><BODY>The time is currently " & DateTime.Now.ToString(DateTimeFormatInfo.CurrentInfo) & "</BODY></HTML>"

                    If DataAMI.Json.Replace(Chr(0), String.Empty) = String.Empty Then
                        DataAMI.Json = """"""
                    End If


                    Dim responseJson As String = "{""data"":" & DataAMI.Json.Replace(Chr(0), String.Empty) & ",""Message"":""" & DataAMI.Message & """,""Status"":""" & DataAMI.Status & """}"
                    Dim buffer() As Byte = System.Text.Encoding.UTF8.GetBytes(responseJson)

                    response.ContentType = "application/json"
                    response.ContentEncoding = System.Text.Encoding.UTF8


                    response.ContentLength64 = buffer.Length

                    Dim output As System.IO.Stream = response.OutputStream

                    output.Write(buffer, 0, buffer.Length)


                Catch ex As HttpListenerException

                    Console.WriteLine(ex.Message)

                Finally

                    If response IsNot Nothing Then

                        response.Close()

                    End If

                End Try

            Next

        Catch ex As HttpListenerException

            Console.WriteLine(ex.Message)

        Finally

            ' Stop listening for requests.

            listener.Close()

            Console.WriteLine("Done Listening...")
        End Try

    End Sub

    Private Sub ProcessRequests(ByVal prefixes() As String)

        If Not System.Net.HttpListener.IsSupported Then

            Console.WriteLine("Windows XP SP2, Server 2003, or higher is required to  & use the HttpListener class.")

            Exit Sub

        End If

        ' URI prefixes are required,

        If prefixes Is Nothing OrElse prefixes.Length = 0 Then

            Throw New ArgumentException("prefixes")

        End If



        ' Create a listener and add the prefixes.

        Dim listener As System.Net.HttpListener = New System.Net.HttpListener()

        For Each s As String In prefixes

            listener.Prefixes.Add(s)

        Next

        Try
            ' Start the listener to begin listening for requests.

            listener.Start()

            Console.WriteLine("Listening...")

            Dim numRequestsToBeHandled As Integer = 10



            For i As Integer = 0 To numRequestsToBeHandled

                Dim response As HttpListenerResponse = Nothing

                Try

                    clearData()

                    ' Note: GetContext blocks while waiting for a request.

                    Dim context As HttpListenerContext = listener.GetContext()

                    Dim IncMessage As New NameValueCollection
                    IncMessage = context.Request.QueryString

                    Dim FieldPID As String = "" '///pid
                    Dim FieldOfficeCode As String = "" '///officeCode
                    Dim FieldVersionCode As String = "" '///versionCode
                    Dim FieldServiceCode As String = "" '///serviceCode

                    Dim FValuePID() As String
                    Dim FValueOfficeCode() As String
                    Dim FValueVersionCode() As String
                    Dim FValueServiceCode() As String

                    If IncMessage.Count = 4 Then
                        If IncMessage.GetKey(0) = "pid" Then
                            FieldPID = IncMessage.GetKey(0) '///pid
                            FValuePID = IncMessage.GetValues(0)
                            DataAMI.PID = FValuePID(0).ToString()
                        End If

                        If IncMessage.GetKey(1) = "officeCode" Then
                            FieldOfficeCode = IncMessage.GetKey(1)  '///pid
                            FValueOfficeCode = IncMessage.GetValues(1)
                            DataAMI.OFFICE_CODE = FValueOfficeCode(0).ToString()
                        End If

                        If IncMessage.GetKey(2) = "versionCode" Then
                            FieldVersionCode = IncMessage.GetKey(2)  '///pid
                            FValueVersionCode = IncMessage.GetValues(2)
                            DataAMI.VERSION_CODE = FValueVersionCode(0).ToString()
                        End If

                        If IncMessage.GetKey(3) = "serviceCode" Then
                            FieldServiceCode = IncMessage.GetKey(3)  '///serviceCode
                            FValueServiceCode = IncMessage.GetValues(3)
                            DataAMI.SERVICE_CODE = FValueServiceCode(0).ToString()
                        End If

                    End If


                    ' Create the response.

                    response = context.Response
                    DataAMI.Status = "N"
                    'ReadCard(DataAMI.PID)

                    Dim responseString As String = "<HTML><BODY>The time is currently " & DateTime.Now.ToString(DateTimeFormatInfo.CurrentInfo) & "</BODY></HTML>"


                    Dim responseJson As String = "{""data"":""" & DataAMI.Json.Replace(Chr(0), String.Empty) & """,""Message"":""" & DataAMI.Message & """,""Status"":""" & DataAMI.Status & """}"
                    Dim buffer() As Byte = System.Text.Encoding.UTF8.GetBytes(responseJson)

                    response.ContentLength64 = buffer.Length

                    Dim output As System.IO.Stream = response.OutputStream

                    output.Write(buffer, 0, buffer.Length)


                Catch ex As HttpListenerException

                    Console.WriteLine(ex.Message)

                Finally

                    If response IsNot Nothing Then

                        response.Close()

                    End If

                End Try

            Next

        Catch ex As HttpListenerException

            Console.WriteLine(ex.Message)

        Finally

            ' Stop listening for requests.

            listener.Close()

            Console.WriteLine("Done Listening...")
        End Try

    End Sub

    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
        Close()
    End Sub

    Private Sub AboutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AboutToolStripMenuItem.Click
        Me.Show()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Hide()
    End Sub

    Private Sub agent_LocationChanged(sender As Object, e As EventArgs) Handles MyBase.LocationChanged
    End Sub

    Private Sub agent_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        Me.Hide()
    End Sub
End Class